
NAME
====

ctys-convertVM - Convert VMs

SYNTAX
======

ctys-convertVM


   [--auto-all]
   [--auto]
   [-C]
   [--create-image]
   [-d <debug-level>]
   [-D <directory>]
   [--defaults-file=<file-name>]
   [--expert]
   [-h]
   [-H <help-options>]
   [--label=<label>]
   [--label <label>]
   [--list-env-var-options|--levo]
   [--no-create-image]
   [--no-save-para-kernel]
   [--save-para-kernel]
   [-V]
   [-X]


ffs.
   [--defaults-file]
   [--defaults-file-create]
   [--defaults-file-create-with-force]
   [-t <session-type>]

DESCRIPTION
===========

Refer to documentation or use following option for
extended help:

     ctys -H help

     displays available online help options.

COPYRIGHT
=========

Copyright (C) 2008, 2009, 2010, 2011 Ingenieurbuero Arno-Can Uestuensoez

For BASE package following licenses apply,

- for software see GPL3 for license conditions,
- for documents see GFDL-1.3 with invariant sections for license conditions,

This document is part of the DOC package,

- for documents and contents from DOC package see 

  'Creative-Common-Licence-3.0 - Attrib: Non-Commercial, Non-Deriv'

  with optional extensions for license conditions.

For additional information refer to enclosed Releasenotes and License files.






