
NAME
====

ctys-vmw2utils - VMW-Server2 utilities

SYNTAX
======

ctys-vmw2utils


   [-d <debug-options>]
   [-h]
   [-H <help-options>]
   [-n]
   [-V]
   [-X]
   [
     (
       (FETCH|F) (
           VMWPATH4OBJID|P4O
          |VMWOBJID4PATH|O4P
          |DATASTORE|D
        )
     )
     |
     (
       (CONVERT|C) (TODATASTORE|2D)
     )
     |
     (
       (LIST|L) (
           INVENTORY|I
          |DATASTORES|D
       )
     )
   ]

DESCRIPTION
===========

Refer to documentation or use following option for
extended help:

     ctys -H help

     displays available online help options.

COPYRIGHT
=========

Copyright (C) 2008, 2009, 2010 Ingenieurbuero Arno-Can Uestuensoez

This is software and documentation from BASE package,

- for software see GPL3 for license conditions,
- for documents  see GFDL-1.3 with invariant sections for license conditions.
The whole document - all sections - is/are defined as invariant.

For additional information refer to enclosed Releasenotes and License files.






