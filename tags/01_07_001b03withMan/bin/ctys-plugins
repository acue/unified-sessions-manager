#!/bin/bash

########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      01_05_001a01
#
########################################################################
#
#     Copyright (C) 2007 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
########################################################################


################################################################
#                   Begin of FrameWork                         #
################################################################


#FUNCBEG###############################################################
#
#PROJECT:
MYPROJECT="Unified Sessions Manager"
#
#NAME:
#  ctys-plugins
#
#AUTHOR:
AUTHOR="Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org"
#
#FULLNAME:
FULLNAME="Unified Sessions Manager"
#
#CALLFULLNAME:
CALLFULLNAME="Check and Manage Plugins"
#
#LICENCE:
LICENCE=GPL3
#
#TYPE:
#  bash-script
#
#VERSION:
VERSION=01_01_001a02
#DESCRIPTION:
#  Main untility of project ctys for manging sessions.
#
#EXAMPLE:
#
#PARAMETERS:
#  Use '-h' for initial help.
#
#OUTPUT:
#  RETURN:
#  VALUES:
#
#FUNCEND###############################################################

################################################################
#                     Global shell options.                    #
################################################################
shopt -s nullglob



################################################################
#       System definitions - do not change these!              #
################################################################
#Execution anchor
MYHOST=`uname -n`
MYCALLPATHNAME=$0
MYCALLNAME=`basename $MYCALLPATHNAME`

if [ -n "${MYCALLPATHNAME##/*}" ];then
    MYCALLPATHNAME=${PWD}/${MYCALLPATHNAME}
fi
MYCALLPATH=`dirname $MYCALLPATHNAME`
###################################################
#load basic library required for bootstrap        #
###################################################
MYBOOTSTRAP=${MYCALLPATH}/bootstrap
if [ ! -d "${MYBOOTSTRAP}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYBOOTSTRAP=${MYBOOTSTRAP}"
cat <<EOF  

DESCRIPTION:
  This directory contains the common mandatory bootstrap functions.
  Your installation my be erroneous.  

SOLUTION-PROPOSAL:
  First of all check your installation, because an error at this level
  might - for no reason - bypass the final tests.

  If this does not help please send a bug-report.

EOF
  exit 1
fi

MYBOOTSTRAP=${MYBOOTSTRAP}/bootstrap.01.01.002
if [ ! -f "${MYBOOTSTRAP}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYBOOTSTRAP=${MYBOOTSTRAP}"
cat <<EOF  

DESCRIPTION:
  This file contains the common mandatory bootstrap functions required
  for start-up of any shell-script within this package.

  It seems though your installation is erroneous or you detected a bug.  

SOLUTION-PROPOSAL:
  First of all check your installation, because an error at this level
  might - for no reason - bypass the final tests.

  When your installation seems to be OK, you may try to set a TEMPORARY
  symbolic link to one of the files named as "bootstrap.<highest-version>".
  
    ln -s ${MYBOOTSTRAP} bootstrap.<highest-version>

  in order to continue for now. 

  Be aware, that any installation containing the required file will replace
  the symbolic link, because as convention the common boostrap files are
  never symbolic links, thus only recognized as a temporary workaround to 
  be corrected soon.

  If this does not work you could try one of the other versions.

  Please send a bug-report.

EOF
  exit 1
fi

###################################################
#Start bootstrap now                              #
###################################################
. ${MYBOOTSTRAP}
###################################################
#OK - utilities to find components of this version#
#available now.                                   #
###################################################

#
#set real path to install, resolv symbolic links
_MYCALLPATHNAME=`bootstrapGetRealPathname ${MYCALLPATHNAME}`
MYCALLPATH=`dirname ${_MYCALLPATHNAME}`
#
###################################################
#Now find libraries might perform reliable.       #
###################################################


#current language, not really NLS
MYLANG=${MYLANG:-en}

#path for various loads: libs, help, macros, plugins
MYLIBPATH=${CTYS_LIBPATH:-`dirname $MYCALLPATH`}

#path for various loads: libs, help, macros, plugins
MYHELPPATH=${MYLIBPATH}/help/${MYLANG}


###################################################
#Check master hook                                #
###################################################
bootstrapCheckInitialPath
###################################################
#OK - Now should work.                            #
###################################################

MYCONFPATH=${MYLIBPATH}/conf/ctys
if [ ! -d "${MYCONFPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYCONFPATH=${MYCONFPATH}"
  exit 1
fi

if [ -f "${MYCONFPATH}/versinfo.conf" ];then
    . ${MYCONFPATH}/versinfo.conf
fi

MYMACROPATH=${MYCONFPATH}/macros
if [ ! -d "${MYMACROPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYMACROPATH=${MYMACROPATH}"
  exit 1
fi

MYPKGPATH=${MYLIBPATH}/plugins
if [ ! -d "${MYPKGPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYPKGPATH=${MYPKGPATH}"
  exit 1
fi

MYINSTALLPATH= #Value is assigned in base. Symbolic links are replaced by target


##############################################
#load basic library required for bootstrap   #
##############################################
. ${MYLIBPATH}/lib/base
. ${MYLIBPATH}/lib/libManager
#
#Germish: "Was the egg or the chicken first?"
#
#..and prevent real load order for later display.
#
bootstrapRegisterLib
baseRegisterLib
libManagerRegisterLib
##############################################
#Now the environment is armed, so let's go.  #
##############################################

if [ ! -d "${MYINSTALLPATH}" ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing:MYINSTALLPATH=${MYINSTALLPATH}"
    gotoHell ${ABORT}
fi

MYOPTSFILES=${MYOPTSFILES:-$MYLIBPATH/help/$MYLANG/*_base_options} 
checkFileListElements "${MYOPTSFILES}"
if [ $? -ne 0 ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing:MYOPTSFILES=${MYOPTSFILES}"
    gotoHell ${ABORT}
fi


################################################################
# Main supported runtime environments                          #
################################################################
#release
TARGET_OS="Linux: CentOS/RHEL(5+), SuSE-Professional 9.3"

#to be tested - coming soon
TARGET_OS_SOON="OpenBSD+Linux(might work for any dist.):Ubuntu+OpenSuSE"

#to be tested - might be almsot OK - but for now FFS
#...probably some difficulties with desktop-switching only?!
TARGET_OS_FFS="FreeBSD+Solaris/SPARC/x86"

#release
TARGET_WM="Gnome + fvwm"

#to be tested - coming soon
TARGET_WM_SOON="xfce"

#to be tested - coming soon
TARGET_WM_FORESEEN="KDE(might work now)"

################################################################
#                     End of FrameWork                         #
################################################################


#
#Verify OS support
#
case ${MYOS} in
    Linux);;
    OpenBSD);;
    SunOS);;
    *)
        printINFO 1 $LINENO $BASH_SOURCE 1 "${MYCALLNAME} is not supported on ${MYOS}"
	gotoHell 0
	;;
esac

if [ "${*}" != "${*//-X/}" ];then
    C_TERSE=1
fi
if [ "${*}" != "${*//-V/}" ];then
    if [ -n "${C_TERSE}" ];then
	echo -n ${VERSION}
    else
	echo "$0: VERSION=${VERSION}"
    fi
    exit 0
fi



#path to directory containing the default mapping db
if [ ! -d "${HOME}/.ctys/db/default" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing standard directory:$MYCONFPATH/groups"
  echo "${MYCALLNAME}:$LINENO:ERROR:Has to be present at least, check your installation"
  exit 1

fi
DEFAULT_DBPATHLST=${DEFAULT_DBPATHLST:-$HOME/.ctys/db/default}

#path to directory containing the default mapping db
if [ -d "${MYCONFPATH}/db/default" ];then
    DEFAULT_DBPATHLST=${DEFAULT_DBPATHLST:-$MYCONFPATH/db/default}
fi


#Source pre-set environment from user
if [ -f "${HOME}/.ctys/ctys.conf" ];then
  . "${HOME}/.ctys/ctys.conf"
fi

#Source pre-set environment from installation 
if [ -f "${MYCONFPATH}/ctys.conf" ];then
  . "${MYCONFPATH}/ctys.conf"
fi

#system tools
if [ -f "${HOME}/.ctys/systools.conf.${MYDIST}" ];then
    . "${HOME}/.ctys/systools.conf.${MYDIST}"
else

    if [ -f "${MYCONFPATH}/systools.conf.${MYDIST}" ];then
	. "${MYCONFPATH}/systools.conf.${MYDIST}"
    else
	if [ -f "${MYCALLPATH}/../conf/ctys/systools.conf.${MYDIST}" ];then
	    . "${MYCALLPATH}/../conf/ctys/systools.conf.${MYDIST}"
	else
	    ABORT=1;
	    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing system tools configuration file:\"systools.conf.${MYDIST}\""
	    printERR $LINENO $BASH_SOURCE ${ABORT} "Check your installation."
	    gotoHell ${ABORT}
	fi
    fi
fi

################################################################
#    Default definitions - User-Customizable  from shell       #
################################################################

###################
#generic
DEFAULT_C_SESSIONTYPE=${DEFAULT_C_SESSIONTYPE:-VNC}
DEFAULT_C_SCOPE=${DEFAULT_C_SCOPE:-USER}
DEFAULT_KILL_DELAY_POWEROFF=${DEFAULT_KILL_DELAY_POWEROFF:-20}
DEFAULT_LIST_CONTENT=${DEFAULT_LIST_CONTENT:-ALL,FULLPATH,BOTH}


###################
#CREATE

###################
#LIST
DEFAULT_C_MODE_ARGS_LIST=${DEFAULT_C_MODE_ARGS_LIST:-"label,id,user,group,pid"}
#DEFAULT_LIST_C_SCOPE="USRLST"
#DEFAULT_LIST_C_SCOPE_ARGS="all"

###################
#ENUMERATE
DEFAULT_C_MODE_ARGS_ENUMERATE=${DEFAULT_C_MODE_ARGS_ENUMERATE:-'.'}

###################
#CREATE
DEFAULT_C_MODE_ARGS=${DEFAULT_C_MODE_ARGS_CREATE:-'1,DEFAULT,REUSE'}



################################################################
#Globalized convenience settings for Basic-Community-Packages  #
################################################################

#Common: Defines the timeout an established port-forwarding tunnel by 
#OpenSSH.
#Should wait for it's first and only one (just an wannabee oneshot - within
#the period any number of connects are possible) connection. This is choosen,
#because because now no precautions have to be and are not implemented for
#cancellation of no longer required tunnels. So you might no set a high value,
#just smallerr than a minute.
#Existing ports are not reused anyway, because the next tunnel-request
#increments the highest present for a new tunnel.
#
#APPLY:Increment when clients do not connect with CONNECTIONFORWARDING.
SSH_ONESHOT_TIMEOUT=${SSH_ONESHOT_TIMEOUT:-20}


#Common: Defines the timeout to delay the start of a client after server
#
#APPLY:Increment this value when clients do not connect.
R_CLIENT_DELAY=${R_CLIENT_DELAY:-2}

#Common: Defines the timeout after all XClients of one desktop are 
#started. Due to problems with reliability a shift of distinguished 
#windows seems not to work(at least in my environment on CentOS-5.0/Gnome).
#So current desktop is switched for default pop-up on target desktop
#gwhich could take some time until the window actually is displayed.
#When the desktop is meanwhile switched the window will be positioned 
#on the current if not yet displayed.
#Depends on actual base, it seems that at least 5seconds are required,
#for safety 8 seconds are choosen.
#
#APPLY:Increment this value when clients pop-up on wrong desktop.
X_DESKTOPSWITCH_DELAY=${X_DESKTOPSWITCH_DELAY:-8}



################################################################
#Basic-Package Settings: VNC
#
#General remarks: 
# The geometry parameter will be reset - for server too - when selected
# at the CLI by "-g" option. So the value here is just a default, when no 
# call parameter is supported.
#

#Bulk: Defines the timeout to wait between bulk creation of sessions 
R_CREATE_TIMEOUT=${R_CREATE_TIMEOUT:-5}


#Bulk: Defines the maximum allowed number of sessions to be created by a bulk call.
#ATTENTION:Mistakenly using e.g. 1000 will probably force you to reboot your machines!!!!
#A call of "ctys -a cancel=all poorHost" might help?!
#
#APPLY:Increment this when more VNC-bulk sessions on a node are required.
R_CREATE_MAX=${R_CREATE_MAX:-20}


################################################################
#      Internal control flow - do not change these!            #
################################################################
C_SESSIONTYPE=DEFAULT
PACKAGES_KNOWNTYPES=

unset C_XTOOLKITOPTS;

unset C_SESSIONID;
unset C_SESSIONIDARGS;
unset C_SESSIONFLAG;
C_MODE=CREATE
#unset C_MODE_ARGS
#C_MODE_ARGS="1,DEFAULT,REUSE"
C_MODE_ARGS=DEFAULT
C_SCOPE=DEFAULT
unset C_SCOPE_ARGS
unset C_SCOPE_CONCAT
C_SSH=1
unset C_EXECLOCAL;
unset C_NOEXEC;
unset C_LISTSES;
unset C_TERSE;
C_PRINTINFO=0;
C_ASYNC=DEFAULT;

unset C_WMC_DESK
unset C_MDESK;
 
unset C_GEOMETRY;
unset C_REMOTERESOLUTION;
unset C_FORCE;
unset C_ALLOWAMBIGIOUS;
#WNG=1;

unset R_HOSTS;
unset R_OPTS;
#unset R_TEXT;
unset X_OPTS;


#Options for formatting text with "pr" when printing out the embedded help.
PR_OPTS=${PR_OPTS:--o 5 -l 76}

#Using this as a ready to use option, some lengthy keywords for now,
#but recognition has priority over string replace-functions!
C_CLIENTLOCATION=${C_CLIENTLOCATION:-"-L DISPLAYFORWARDING"};


#Base for remapping of local client access ports for CONNECTIONFORWARDING
LOCAL_PORTREMAP=${LOCAL_PORTREMAP:-5950}

#Is defined to be used when set, so it is foreseen as test-path for remote call
#R_PATH

#Is defined to be used when set, so it is foreseen as test-path for local call
#L_PATH


#assure for append...
if [ -n "$CTYS_GROUPS_PATH" ];then
    mstr=$HOME/.ctys/groups
    CTYS_GROUPS_PATH=${CTYS_GROUPS_PATH//$mstr}
    mstr=$MYCONFPATH/groups
    CTYS_GROUPS_PATH=${CTYS_GROUPS_PATH//$mstr}
fi

if [ -n "$CTYS_GROUPS_PATH" ];then
    checkPathElements CTYS_GROUPS_PATH ${CTYS_GROUPS_PATH}
fi

if [ ! -d "${HOME}/.ctys/groups" ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing standard directory:${HOME}/.ctys/groups"
    printERR $LINENO $BASH_SOURCE ${ABORT} "Has to be present at least, check your installation"
    gotoHell ${ABORT}
fi

if [ ! -d "${MYCONFPATH}/groups" ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing standard directory:${MYCONFPATH}/groups"
    printERR $LINENO $BASH_SOURCE ${ABORT} "Has to be present at least, check your installation"
    gotoHell ${ABORT}
fi

CTYS_GROUPS_PATH="${HOME}/.ctys/groups:${MYCONFPATH}/groups${CTYS_GROUPS_PATH:+:$CTYS_GROUPS_PATH}"




################################################################
#                    Initial call trace                        #
################################################################
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE ""
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "-----------------------------------------------------------------------------"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "TRACE:CALL-PARAMS:"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "<${0} ${*}>"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "-----------------------------------------------------------------------------"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "TRACE:ENV-PARAMS:"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "<PATH=${PATH}>"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "<LD_PLUGIN_PATH=${LD_PLUGIN_PATH}>"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "<RS_PREFIX_L=${RS_PREFIX_L}>"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "<RS_PREFIX_R=${RS_PREFIX_R}>"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE "-----------------------------------------------------------------------------"
printDBG $S_BIN ${D_FLOW} $LINENO $BASH_SOURCE ""




#################################################################################
#load libraries and plugins                                                     #
#################################################################################
#
#Remark: The online help is loaded only when requested by user.
#        The runtime modules are loaded "by-startup-scan" not on demand.
#
#        - BASE-libs and CORE-plugins with related help:
#          are loaded - due to beeing prerequisite - as hardcoded entities.
#          Related help is loaded when requested, but on-demand as said.
#
#        - ADD-ON-PACKAGES
#          Loaded "by-startup-scan".
#
#        - ADD-ON-MACROS
#          CONFIG-MACROS: Loaded as given by User-Option.
#

#########################################################################
#libraries - generic functions                                          #
#########################################################################

#These will be hardcoded and just sourced, but are completely unmanaged.
. ${MYLIBPATH}/lib/cli/cli
. ${MYLIBPATH}/lib/misc
. ${MYLIBPATH}/lib/security
. ${MYLIBPATH}/lib/help/help
. ${MYLIBPATH}/lib/geometry/geometry
. ${MYLIBPATH}/lib/wmctrlEncapsulation
. ${MYLIBPATH}/lib/network/network



#########################################################################
#plugins - project specific commons and feature plugins                 #
#########################################################################


PLUGINPATHS=${MYINSTALLPATH}/plugins/CORE
PLUGINPATHS=${PLUGINPATHS}:${MYINSTALLPATH}/plugins/GENERIC
PLUGINPATHS=${PLUGINPATHS}:${MYINSTALLPATH}/plugins/HOSTs
PLUGINPATHS=${PLUGINPATHS}:${MYINSTALLPATH}/plugins/VMs
PLUGINPATHS=${PLUGINPATHS}:${MYINSTALLPATH}/plugins/PMs
PLUGINPATHS=${PLUGINPATHS}:${MYINSTALLPATH}/plugins/GUESTs

LD_PLUGIN_PATH=${LD_PLUGIN_PATH}:${PLUGINPATHS}



function 0_initPlugins ()  {
    #
    #Perform the initialization according and based on LD_PLUGIN_PATH.
    #
    echo
    echo "------------------------------"
    echo "Start check on:${MYHOST}"
    echo "------------------------------"
    echo "Prepare check on:${MYHOST}"
    echo "------------------------------>>>"
    MYROOTHOOK=${MYINSTALLPATH}/plugins/hook
    if [ ! -f "${MYROOTHOOK}" ];then 
	ABORT=2
	printERR $LINENO $BASH_SOURCE ${ABORT} "Missing packages hook: hook=${MYROOTHOOK}"
	gotoHell ${ABORT}
    fi
    echo "<<<------------------------------"
    echo "Checking PLUGINS-STATEs on:${MYHOST}"
    echo "Perform:${MYROOTHOOK}"
    echo "------------------------------>>>"
    . ${MYROOTHOOK}
    initPackages "${MYROOTHOOK}"
    echo "<<<------------------------------"
    echo "...results on:${MYHOST} to:"
    echo
}

function 1_pluginsEnumerate () {
    local _knownTypes="`hookGetKnownTypes`"
    local _disabledTypes="`hookGetDisabledTypes`"
    local _ignoredTypes="`hookGetIgnoredTypes`"

    local _allign=5;
    local _label=12;
    local _label1=23;

    if [ -z "$C_EXECLOCAL" ];then
	printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	    " " "Check-Client/Server" "`setSeverityColor TRY CLIENT features`"
    else
	printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	    " " "Check-Client/Server" "`setSeverityColor TRY SERVER features`"
    fi
    echo

    printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	" " "Host" "${MYHOST}"

    printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	" " "OS" "${MYOS}"

    printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	" " "OS-Release" "${MYOSREL}"

    printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	" " "Distribution" "${MYDIST}"

    printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	" " "Release" "${MYREL}"

    echo

    printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	" " "PLUGINS States" "As requested by \"-t\" and \"-T\", CORE and GENERIC are not handled here"

    printf "%"$((1*_allign))"s%-"${_label}"s %-"$((2*_allign))"s\n" \
	" " "" "\"${MYCALLNAME} ${CALLARGS}\""

    echo

    #IGNORED(-)
    printf "%"$((2*_allign))"s%-"${_label1}"s%-"$((2*_allign))"s\n" \
	" " "`setStatusColor IGNORED IGNORED`(-):" "`setStatusColor IGNORED ${_ignoredTypes}`"

    #AVAILABLE(0)
    printf "%"$((2*_allign))"s%-"${_label1}"s%-"$((2*_allign))"s\n" \
	" " "`setStatusColor AVAILABLE AVAILABLE`(0):" "`setStatusColor AVAILABLE ${_knownTypes}`"

    #DISABLED(1)
    printf "%"$((2*_allign))"s%-"${_label1}"s%-"$((2*_allign))"s\n" \
	" " "`setStatusColor DISABLED DISABLED`(1):" "`setStatusColor DISABLED ${_disabledTypes}`"


    #ENABLED(2)
    local _curEnabled=$_knownTypes;
    for i in ${_disabledTypes};do
	_curEnabled=${_curEnabled//$i}
    done
    printf "%"$((2*_allign))"s%-"${_label1}"s%-"$((2*_allign))"s\n" \
	" " "`setStatusColor ENABLED ENABLED`(2):" "`setStatusColor ENABLED ${_curEnabled}`"

    #IDLE(3)
    local _curIdle=;
    if [ "${C_SESSIONTYPE}" == ALL ];then
	_curIdle="";
    else
	_curIdle=${_curEnabled//$C_SESSIONTYPE};
    fi

    #BUSY(4)
    local _curBusy=;
    if [ "${C_SESSIONTYPE}" == ALL ];then
	_curBusy=${_curEnabled};
    else
	local _t=`echo " $CALLARGS "| sed -n 's/^.* -t/-t/;s/-t \([a-zA-Z0-9]*\) .*$/\1/p'`
	if [ -n "${_t}" ];then
	    _curBusy=`echo "${_t}"|tr '[:lower:]' '[:upper:]'`
	else
	    _curBusy=${C_SESSIONTYPE};
	fi
    fi
    for i in ${_disabledTypes};do
	_curBusy=${_curBusy//$i/${i}\(DISABLED)}
    done
    _curIdle=${_curEnabled//$_curBusy};

    printf "%"$((2*_allign))"s%-"${_label1}"s%-"$((2*_allign))"s\n" \
	" " "`setStatusColor IDLE IDLE`(3):" "`setStatusColor IDLE ${_curIdle}`"

    printf "%"$((2*_allign))"s%-"${_label1}"s%-"$((2*_allign))"s\n" \
	" " "`setStatusColor BUSY BUSY`(4):" "`setStatusColor BUSY ${_curBusy}`"

    echo
    printf "%"$((1*_allign))"s%-"${_label}"s:%-"$((2*_allign))"s\n" \
	" " "PLUGINS basic info" ""
    echo
    for _ty in ${_knownTypes};do
	eval info${_ty} $_allign 
    done

    echo "------------------------------"
    echo "End check on:${MYHOST}"
    echo "------------------------------"
}




_ARGS=;
_ARGSCALL=$*;


while [ -n "$1" ];do
    printDBG $S_LIB ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:\${1}=<${1}>"
    case $1 in
	'-h')showToolHelp;exit 0;;
	'-V')printVersion;exit 0;;
	'-d')shift;;
	'-T')shift;;
	'-t')shift;;
	'-E')
	    C_EXECLOCAL=1;
	    printDBG $S_BIN ${D_UID} $LINENO $BASH_SOURCE "$FUNCNAME:C_EXECLOCAL"
	    ;;

	*)
	    _ARGS="${_ARGS} ${1}"
	    printDBG $S_LIB ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:_ARGS=${_ARGS}"
	    ;;
    esac
    shift
done

_RARGS=${_ARGSCALL//$_ARGS/}
_MYLBL=${MYCALLNAME}-${MYUID}-${DATE}

printDBG $S_LIB ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:_ARGS =<$_ARGS>"
printDBG $S_LIB ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:_RARGS=<$_RARGS>"
_RARGS=${_RARGS// /\%}
printDBG $S_LIB ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:_RARGS=<$_RARGS>"
printDBG $S_LIB ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:_MYLBL=<$_MYLBL>"

if [ -z "${_ARGS}" ];then
    _t=`echo " $_ARGSCALL "| sed -n 's/(.*)//g;s/^.* -T/-T/;s/-T \([a-zA-Z0-9,]*[^ ]\).*$/\1/p'`
    if [ -n "${_t}" ];then
	CTYS_MULTITYPE=`echo "${_t}"|tr '[:lower:]' '[:upper:]'`
	printDBG $S_BIN ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:CTYS_MULTITYPE=$CTYS_MULTITYPE"
    fi
    if [ -z "${CTYS_MULTITYPE//*DEFAULT*/}" ];then
	_ac=`echo " $_ARGSCALL "| sed -n 's/^.* -a/-a/;s/-a \([a-zA-Z0-9]*\)[= ].*$/\1/p'|tr '[:lower:]' '[:upper:]'`
	_ty=`echo " $_ARGSCALL "| sed -n 's/^.* -t/-t/;s/-t \([a-zA-Z0-9]*\) .*$/\1/p'|tr '[:lower:]' '[:upper:]'`
	if [ -n "${_ty}" ];then
	    CTYS_MULTITYPE="${_ty}"
	fi
	printDBG $S_BIN ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:C_SESSIONTYPE=$C_SESSIONTYPE"
    fi

    0_initPlugins
    1_pluginsEnumerate
else
    _argChk=`echo " ${_ARGS} "|sed 's/["'\'']([^)]*)["'\'']//g'`
    _argChk=" ${_argChk} "
    _argChk1="${_argChk// -/}"
    if [ "${_argChk}" != "${_argChk1}" ];then
	printERR $LINENO $BASH_SOURCE 1 "Local options in remote host section:${_argChk}"
	printERR $LINENO $BASH_SOURCE 1 "For local options only the original ${MYCALLNAME} options are permitted."
	gotoHell 1;
    fi
    printINFO 1 $LINENO $BASH_SOURCE 1 "Remote execution on:${_ARGS}"
    ctys ${C_DARGS} -t cli -a create=l:${_MYLBL},cmd:${MYCALLNAME}%${_RARGS} ${_ARGS}
fi


