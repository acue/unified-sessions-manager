#!/bin/bash

########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      01_05_001a01
#
########################################################################
#
#     Copyright (C) 2007 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
########################################################################


################################################################
#                   Begin of FrameWork                         #
################################################################


#FUNCBEG###############################################################
#
#PROJECT:
MYPROJECT="Unified Sessions Manager"
#
#NAME:
#  ctys-extractMAClst
#
#AUTHOR:
AUTHOR="Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org"
#
#FULLNAME:
FULLNAME="CTYS Map MAC-Addresses to TCP/IP Names and Addresses"
#
#CALLFULLNAME:
CALLFULLNAME="ctys-macmap"
#
#LICENCE:
LICENCE=GPL3
#
#TYPE:
#  bash-script
#
#VERSION:
VERSION=01_01_001b01
#DESCRIPTION:
#  Generated a sorted list of output for a matchin regexpr.
#
#
#EXAMPLE:
#
#PARAMETERS:
#  -n|-i|-m
#    -n  Print: <name>
#    -i  Print: <IP>
#    -m  Print: <MAC>
#
#  -h
#     Print help.
#
#  -V
#     Version.
#
#  -p <db-dir-path-list>
#     Two syntaxes are supported for <db-dir-path-list>:
#
#       - PATH-like with FS=':':
#         <db-dir-path-list>=<db-dir-path>[:<db-dir-path>[:...]]
#
#       - ctys-conformant with FS='%', due to leveled scanner-hierarchies:
#         <db-dir-path-list>=<db-dir-path>[%<db-dir-path>[%...]]
#
#     So, both supported field seperators are reserved values.
#
#     Directory paths for databases, containing a file with hard-coded name "macmap.fdb".
#     Default is to use DEFAULT_DBPATHLST.
#
#
#     ATTENTION: Currently no spaces are supported within pathnames!
#
#
#  <simple-awk-regexpr>
#
#
#OUTPUT:
#  RETURN:
#  VALUES:
#
#FUNCEND###############################################################


################################################################
#                     Global shell options.                    #
################################################################
shopt -s nullglob



################################################################
#       System definitions - do not change these!              #
################################################################
#Execution anchor
MYHOST=`uname -n`
MYCALLPATHNAME=$0
MYCALLNAME=`basename $MYCALLPATHNAME`

if [ -n "${MYCALLPATHNAME##/*}" ];then
    MYCALLPATHNAME=${PWD}/${MYCALLPATHNAME}
fi
MYCALLPATH=`dirname $MYCALLPATHNAME`
###################################################
#load basic library required for bootstrap        #
###################################################
MYBOOTSTRAP=${MYCALLPATH}/bootstrap
if [ ! -d "${MYBOOTSTRAP}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYBOOTSTRAP=${MYBOOTSTRAP}"
cat <<EOF  

DESCRIPTION:
  This directory contains the common mandatory bootstrap functions.
  Your installation my be erroneous.  

SOLUTION-PROPOSAL:
  First of all check your installation, because an error at this level
  might - for no reason - bypass the final tests.

  If this does not help please send a bug-report.

EOF
  exit 1
fi

MYBOOTSTRAP=${MYBOOTSTRAP}/bootstrap.01.01.002
if [ ! -f "${MYBOOTSTRAP}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYBOOTSTRAP=${MYBOOTSTRAP}"
cat <<EOF  

DESCRIPTION:
  This file contains the common mandatory bootstrap functions required
  for start-up of any shell-script within this package.

  It seems though your installation is erroneous or you detected a bug.  

SOLUTION-PROPOSAL:
  First of all check your installation, because an error at this level
  might - for no reason - bypass the final tests.

  When your installation seems to be OK, you may try to set a TEMPORARY
  symbolic link to one of the files named as "bootstrap.<highest-version>".
  
    ln -s ${MYBOOTSTRAP} bootstrap.<highest-version>

  in order to continue for now. 

  Be aware, that any installation containing the required file will replace
  the symbolic link, because as convention the common boostrap files are
  never symbolic links, thus only recognized as a temporary workaround to 
  be corrected soon.

  If this does not work you could try one of the other versions.

  Please send a bug-report.

EOF
  exit 1
fi

###################################################
#Start bootstrap now                              #
###################################################
. ${MYBOOTSTRAP}
###################################################
#OK - utilities to find components of this version#
#available now.                                   #
###################################################

#
#set real path to install, resolv symbolic links
_MYCALLPATHNAME=`bootstrapGetRealPathname ${MYCALLPATHNAME}`
MYCALLPATH=`dirname ${_MYCALLPATHNAME}`
#
###################################################
#Now find libraries might perform reliable.       #
###################################################


#current language, not really NLS
MYLANG=${MYLANG:-en}

#path for various loads: libs, help, macros, plugins
MYLIBPATH=${CTYS_LIBPATH:-`dirname $MYCALLPATH`}

#path for various loads: libs, help, macros, plugins
MYHELPPATH=${MYLIBPATH}/help/${MYLANG}


###################################################
#Check master hook                                #
###################################################
bootstrapCheckInitialPath
###################################################
#OK - Now should work.                            #
###################################################

MYCONFPATH=${MYLIBPATH}/conf/ctys
if [ ! -d "${MYCONFPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYCONFPATH=${MYCONFPATH}"
  exit 1
fi

if [ -f "${MYCONFPATH}/versinfo.conf" ];then
    . ${MYCONFPATH}/versinfo.conf
fi

MYMACROPATH=${MYCONFPATH}/macros
if [ ! -d "${MYMACROPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYMACROPATH=${MYMACROPATH}"
  exit 1
fi

MYPKGPATH=${MYLIBPATH}/plugins
if [ ! -d "${MYPKGPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYPKGPATH=${MYPKGPATH}"
  exit 1
fi

MYINSTALLPATH= #Value is assigned in base. Symbolic links are replaced by target


##############################################
#load basic library required for bootstrap   #
##############################################
. ${MYLIBPATH}/lib/base
. ${MYLIBPATH}/lib/libManager
#
#Germish: "Was the egg or the chicken first?"
#
#..and prevent real load order for later display.
#
bootstrapRegisterLib
baseRegisterLib
libManagerRegisterLib
##############################################
#Now the environment is armed, so let's go.  #
##############################################

if [ ! -d "${MYINSTALLPATH}" ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing:MYINSTALLPATH=${MYINSTALLPATH}"
    gotoHell ${ABORT}
fi

MYOPTSFILES=${MYOPTSFILES:-$MYLIBPATH/help/$MYLANG/*_base_options} 
checkFileListElements "${MYOPTSFILES}"
if [ $? -ne 0 ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing:MYOPTSFILES=${MYOPTSFILES}"
    gotoHell ${ABORT}
fi


################################################################
# Main supported runtime environments                          #
################################################################
#release
TARGET_OS="Linux: CentOS/RHEL(5+), SuSE-Professional 9.3"

#to be tested - coming soon
TARGET_OS_SOON="OpenBSD+Linux(might work for any dist.):Ubuntu+OpenSuSE"

#to be tested - might be almsot OK - but for now FFS
#...probably some difficulties with desktop-switching only?!
TARGET_OS_FFS="FreeBSD+Solaris/SPARC/x86"

#release
TARGET_WM="Gnome + fvwm"

#to be tested - coming soon
TARGET_WM_SOON="xfce"

#to be tested - coming soon
TARGET_WM_FORESEEN="KDE(might work now)"

################################################################
#                     End of FrameWork                         #
################################################################


#
#Verify OS support
#
case ${MYOS} in
    Linux);;
    OpenBSD);;
    SunOS);;
    *)
        printINFO 1 $LINENO $BASH_SOURCE 1 "${MYCALLNAME} is not supported on ${MYOS}"
	gotoHell 0
	;;
esac


. ${MYLIBPATH}/lib/help/help

#path to directory containing the default mapping db
if [ -d "${HOME}/.ctys/db/default" ];then
    DEFAULT_DBPATHLST=${DEFAULT_DBPATHLST:-$HOME/.ctys/db/default}
fi

#path to directory containing the default mapping db
if [ -d "${MYCONFPATH}/db/default" ];then
    DEFAULT_DBPATHLST=${DEFAULT_DBPATHLST:-$HOME/conf/db/default}
fi


#Source pre-set environment from user
if [ -f "${HOME}/.ctys/ctys.conf" ];then
  . "${HOME}/.ctys/ctys.conf"
fi

#Source pre-set environment from installation 
if [ -f "${MYCONFPATH}/ctys.conf" ];then
  . "${MYCONFPATH}/ctys.conf"
fi

#system tools
if [ -f "${HOME}/.ctys/systools.conf.${MYDIST}" ];then
    . "${HOME}/.ctys/systools.conf.${MYDIST}"
else

    if [ -f "${MYCONFPATH}/systools.conf.${MYDIST}" ];then
	. "${MYCONFPATH}/systools.conf.${MYDIST}"
    else
	if [ -f "${MYCALLPATH}/../conf/ctys/systools.conf.${MYDIST}" ];then
	    . "${MYCALLPATH}/../conf/ctys/systools.conf.${MYDIST}"
	else
	    ABORT=1;
	    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing system tools configuration file:\"systools.conf.${MYDIST}\""
	    printERR $LINENO $BASH_SOURCE ${ABORT} "Check your installation."
	    gotoHell ${ABORT}
	fi
    fi
fi

################################################################
#    Default definitions - User-Customizable  from shell       #
################################################################

printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "Let's go..."

argLst=;
while [ -n "$1" ];do
    case $1 in
	'-i')_ip=1;shift;;
	'-m')_mac=1;shift;;
	'-n')_name=1;shift;;
	'-p')shift;_dbfilepath=$1;shift;;
	'-X')C_TERSE=1;shift;;
	'-d')shift;DBG=1;shift;;
	'-V')printVersion;exit 0;;
	'-h')showToolHelp;exit 0;;
        *)argLst="${argLst} $1";shift;;
    esac
done
if [ -z "$_ip" -a -z "$_mac" -a -z "$_name" ];then _ip=1;_mac=1;_name=1;fi
argLst="${argLst## }";
argLst="${argLst%% }";


#match from all, build cat-list
_fdblst=;
if [ -n "$_dbfilepath" ];then
    _dbfilepath=${_dbfilepath//:/ }
    _dbfilepath=${_dbfilepath//%/ }

    for i in ${_dbfilepath};do
        if [ -d "${i}" ];then
            if [ -f "${i}/macmap.fdb" ];then
		_fdblst="${_fdblst} $i/macmap.fdb";
	    fi
	fi
    done
else
    dbfilepath=;
    for i in ${DEFAULT_DBPATHLST//:/ };do
        if [ -d "${i}" ];then
            if [ -f "${i}/macmap.fdb" ];then
		_fdblst="${_fdblst} $i/macmap.fdb";
	    fi
	fi
    done
fi

if [ -z  "${_fdblst}" ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing macmap.fdb"
    gotoHell ${ABORT}
fi

cat ${_fdblst}|\
sort|\
awk -F';' -v s="${argLst}" -v i=$_ip -v m=$_mac -v n=$_name \
          '
          BEGIN       {cache="";mx=0;}
          $0~s&&n==1  {cache=cache $1;  mx=1;}
          $0~s&&m==1  {if(mx==1)cache=cache ";";cache=cache $2;  mx=1;}
          $0~s&&i==1  {if(mx==1)cache=cache ";";cache=cache $3;  mx=1;}
          mx==1       {printf("%s\n",cache);cache="";mx=0;}
         '
