#!/bin/bash

########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@users.sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      01_02_007a17
#
########################################################################
#
# Copyright (C) 2007 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
########################################################################

_myPKGNAME_QEMU_CONFIG="${BASH_SOURCE}"
_myPKGVERS_QEMU_CONFIG="01.01.001a01pre"
hookInfoAdd $_myPKGNAME_QEMU_CONFIG $_myPKGVERS_QEMU_CONFIG
_myPKGBASE_QEMU_CONFIG="`dirname ${_myPKGNAME_QEMU_CONFIG}`"




