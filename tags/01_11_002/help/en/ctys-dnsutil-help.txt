
NAME
====

ctys-dnsutil - supports display and validation of DNS data

SYNTAX
======

ctys-dnsutil


   [-c]
   [-C]
   [-d <debug-level>]
   [-h]
   [-H <help-options>]
   [-i]
   [-l <USER>]
   [-L <remote-user>]
   [-n]
   [--reverse <runtime states> 
         =
         [(REVERSE|R|-),]
         PING|SSH
         [,PM|VM]
       ]
   [-R <remote-host>]
   [-V]
   [-X]
   [<dns-server-list>]



DESCRIPTION
===========

Refer to documentation or use following option for
extended help:

     ctys -H help

     displays available online help options.

COPYRIGHT
=========

Copyright (C) 2008, 2009, 2010 Ingenieurbuero Arno-Can Uestuensoez

This is software and documentation from BASE package,

- for software see GPL3 for license conditions,
- for documents  see GFDL-1.3 with invariant sections for license conditions.

For additional information refer to enclosed Releasenotes and License files.






