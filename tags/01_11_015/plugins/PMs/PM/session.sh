#!/bin/bash

########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      01_02_007a17
#
########################################################################
#
# Copyright (C) 2007 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
########################################################################

_myPKGNAME_PM_SESSION="${BASH_SOURCE}"
_myPKGVERS_PM_SESSION="01.01.001a00"
hookInfoAdd $_myPKGNAME_PM_SESSION $_myPKGVERS_PM_SESSION



#Even though currently empty, kept as reminder for standard file structure.
