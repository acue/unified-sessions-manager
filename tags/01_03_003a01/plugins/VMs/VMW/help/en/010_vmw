   9.3.2. VMMW - VMware
   ====================

   9.3.2.1. Descrition
    ------------------

    This plugin adds support for VMW sessions. The current supported
    client type of this package is the native GUI provided by VMware.

    Any not listed standard option of ctys applies as defined. In
    cases of divergent behaviour for similiar options, and  options
    with specific suboptions are are listed in this section. 


    REMARK: 

       Even though the products of VMware are not OpenSource, at least
       not as a whole, I personally own several commercial and free
       licenses, use them frequently and don't want to miss them.

       The products are used perfectly in almost the same manner as a
       so called "not-open-too-PC" on a unified Linux-Based
       distributed network. Where my network IS the COMPUTER and
       almost anything else than Linux is just a roaming virtual
       device. Within a "dynamic VLAN" of course.

       Which is completed by OpenBSD as the choice of my diskless
       "embedded" networking equipment.


       Therefore I decided to forward this bridging plugin as one of
       the building blocks for my other upcoming projects.


       My most important applications of VMware are the 

          final migration of Windows-Applications to HW independent
          unified roaming appliances, 

       and 

          Cross-Build of Open/Free-Source OSs, which works perfectly -
          even though some performance lossess has to be accepted.


       I even managed to boot and run a received Linux-Application as
       a VirtualPC appliance with a Linux guest for a Windows host,
       running it within a VMware-WS on a Linux host within a Windows
       guest. On a Pentium-4-1,8GHz with 512MByte SDRAM. This was done
       for some earlier versions, but not continued.


    Current supported products are:

    - VMware-WS      (latest ctys versions tested with WS6+)
    - VMware-Server  (tested with 1.0.3+, 1.0.x+ might work)
    - VMware-Player  (to be tested)

    



    PREREQUISITES/RECOMMENDATIONS:

      1. For products requiering explicit setting of independent
         background operations for server, when client terminates, 
         this has to be set. This is not required for server-products,
         but should be done for workstation products.

         Not setting this leads to an immediate temination of the
         server, when client closes. Not neccessarily with a 
         soft-shutdown!!!

      2. The tabs-mode for the proprietary CONSOLE should be set off.
         Even though a tabbed-view could be used too.

         Due to the embedded dispatcher for the display the CONSOLE
         requires here a user interaction for selecting the target
         display in any case(by tabs, or by menu "tabs"), but when
         only one display per window is assigned it appears to be
         little more straight-forward.

      3. A user authentication is for "-P <port>" access even for "-h
         localhost" required, so for ConnectionForwarding in any case
         the user seems to have to perform a login. In addition, it
         seems that the user has to be a local user on that machine.

         As far as I can say for now, only in case of
         DisplayForwarding SSO has an effect on sessions. Let me
         please know, when this is wrong, and don't forget to explain
         how.


   9.3.2.2. Options
    ---------------

    -a <action>[=<action-suboptions>]

        CANCEL=(<machine-address>){1}|all
                    (
                      [,CLIENT]
                      |
                      (
                        [,SERVER|BOTH]
                        [,FLAT|STACK]
                        [,INIT:<init-state>]
                        [,SUSPEND|RESET|REBOOT
                         |(POWEROFF[:<timeoutBeforeKillVM>)]]
                      )
                      [,FORCE]
                      [SYNCTIMEOUT:<timeout-value>]
                    )

            Following additional suboptions apply:

              - CLIENT
                When this keyword is present only the client processes
                of the previous set are filtered.

                This is not applicable for VMplayer.
  
              - SERVER
                When this keyword is present only the server processes
                of the previous set are filtered.

                This is not applicable for VMplayer.

              - BOTH
                When this keyword is present the server and client
                processes of the previous set are filtered.


        CREATE

            For this version there the limiting call constraint is,
            that only one <vm-address> per target could be used. This
            is one for the top-level/global "-a" option, and one for
            each host-context "host01'(-a ...)'".

            This will be extended within one of the next versions.

            VM or Team to be loaded and options passed through.
            For additional information refer to the related documentation 
            from VMware. E.g. "Workstation User's Manual " Workstation 
            6.0 Appendix A "Workstation  Command-Line Reference"; pg. 403.


             - (pathname|pname|p):<vmx-path>

               The vmx-path is a fully qualitfied filename with it's
               path-prefix, which could be a relative or an absolute
               path.

               This is used by the vmware tools "vmware" and "vmrun"
               as a the locally unique identifier on the current host
               for the virtual machine. If the value is present the
               named VM will be loaded.

               <vmx-path> could be <path_to_virtual_machine>, or
               <path_to_virtual_team>, or some convenient shortcuts to
               be expanded as described at the end of this document.

             - <callopts> 

               When <callopts> are given, these will be passed
               through to the

                  "vmware <callopts> <vmx-path>"

               call.  

               The relevant important options are:
               -n   New window(WS only).
               -X   New window in full screen mode.
               -t   New Tab(WS only).
               -x   PowerOn immediately.
               -q   Cose VM when PowerOff, and if last,then close 
                    Window too.
               -s   Set specified config variable.


             - <xopts> 

               XToolkit options, when are given, these will be passed
               through to the

                  "vmware <callopts> <vmx-path> -- <xopts>"

               call. The required "--" double hyphen will be inserted
               as required.

               Be aware, that some of these such as "-geometry" and
               "-name" are already implicitly utilized by other
               options, thus use this if, than CAREFULLY.


             - VNCVIEWER|VNC

               Uses "vncviewer" as it's GUI client. This is currently
               supoorted by WS6 only, and has to be preconfigured for
               the VM. Due to the required fixed port assignment to
               the VM, some coordination with ranges of dynamically
               assigned ports by VNC is required.



         ENUMERATE
            Enumerates all VMW sessions, therefore the vmx-files will
            be scanned and the matched attributes displayed. 

            Therefore the following order of files will be scanned for
            values:
 
              1. <pname>
                 The standard configuration file for VM, as given.

              2. <pname-prefix>.ctys
                 The prefix of given filename with the ".ctys" suffix.

              3. <pname-dirname>.ctys
                 The dirname of given file with ".ctys" suffix.

            In each case the searched key is expected to have the
            prefix "#@#" within the file.


    -g <geometry>|<geometryExtended>

       The geometry has a slightly different behaviour to the standard
       when specific options of propriatary WS-CONSOLE are switched
       on.

       - The positioning parts of parameters of <geometryExtended>
         seem to work in any case correctly. 

       - The offset of <geometry> seems to work proper in any case too.

       - The size of <geometry> seems to work proper when
         "AutofitWindow" and "AutofitGuest" are switched off. 

         As could be seen, shortly after start of CONSOLE it will 
         resizes itself, if previous parameters are set.

         Which is indeed a pretty well behaviour. What else should
         that options of CONSOLE control?



    -L (LocalOnly|LO)
       | (ConnectionForwarding|CF)
       | (DisplayForwarding|DF)
       | (ServerOnly|SO)


        Currently the following selections are supported:


                                |LO |CF |DF |SO
           ---------------------+---+---+---+--
           VMware-WS            |   |no |yes| 1)
           VMware-Server        |   |yes|yes| 1)
           VMware-Player        |   |   |   |


           1) The background-server-mode is currently implicitly 
              supported only. 

              This requires the options to be selected within the
              products - which are slightly different, but are almost
              commonly supported - and will be implicitly started only
              when starting the whole product. 

              The server component continues execution when the client
              is canceled, but could not be started separately.

              A CONNECT to a running server is supported.
