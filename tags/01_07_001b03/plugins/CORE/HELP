#!/bin/bash

########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      01_02_007a17
#
########################################################################
#
# Copyright (C) 2007 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
########################################################################

_myPKGNAME_COREHELP="${BASH_SOURCE}"
_myPKGVERS_COREHELP="01.02.002c01"
hookInfoAdd "$_myPKGNAME_COREHELP" "$_myPKGVERS_COREHELP"


#FUNCBEG###############################################################
#NAME:
#  printHelp
#
#TYPE:
#  bash-function
#
#DESCRIPTION:
#
#EXAMPLE:
#
#PARAMETERS:
#
#
#OUTPUT:
#  RETURN:
#
#  VALUES:
#
#FUNCEND###############################################################
function printHelp () {
printVersion
cat << END2

usage: ${MYCALLNAME} [${OPTLST}] \\
        [--]                                                   \\
        [(<any-options global for all remote>)]                \\
        [user@]<hostname>[(<any-options from here on>)][ ...]  

  Some important options to start are:

    ${MYCALLNAME} \\
        [-t <session_type>[=<session-subopts>]]           \\
        [-a <action>[=<action-subopts>]]                  \\
        [-D <desktop-id>|<desktop-name>]                  \\
        [-g <geometryExtended>|<gridMode>]                \\
        [-r <xsize>x<ysize>]                              \\
        [--]                                              \\
        [(<any-options global for all remote>)]           \\
        [user@]<hostname|groupname>[(<any-options>)][ ...]        


    - <session_type>
      Flexible generic Plugin-Design for almost any feature,
      particularly currently supported plugins are:

      - CLI, X11, VNC, VMW, XEN, PM

      Soon following:

      -  QEMU

    - <action>
      Generic commands for common management and operations of
      multiple physical and virtual host on multiple-display single
      and groups of workstations.

    - <geometry><geometryExtended>
      Extended version of common X11 geometry parameter with
      additionally support of arbitrary ServerLayout sections from
      /etc/X11/xorg.conf. Screens could be addressed by their labels
      or indexes or by ordinary pixel-calculation.

    - <desktop-id>|<desktop-name>
      Multiple desktops could be addressed seamless and globally or 
      specific within each hosts context parameters.

      The usage of this option requires the external tool "wmctrl".

      Stateless Xserver/Xclient access is supported by subplugins.
      Distributed desktops are supported by means of Xdmx as a 
      virtual Xinerama mode.

    - <xsize>x<ysize>
      This is the desktop resolution provided by the server. When 
      supported, it could be set different to the resolution of the 
      client component.

    - <any-options>
      Any global option could be provided individually for each host.
      One implementation specific to be aware of is, that these options 
      are superposed, but not reset, thus the current environment will 
      remain for the following host.



  ATTENTION:
     "--" is required when using remote options, otherwise some
     problems with standard options might occur.


  Layout - Remote-Sessions - Virtual-Machines
    Given the current script interface in combination with tools like 
    gconftool and wmctrl and xdmx, almost any desktop layout could be 
    implemented as a recallable shell script. Particularly for usage 
    in login scripts for enhanced setup of distributed environments with 
    various Desktop clients. 

    This is particularly true for automatic remote logins
    and/or boot of arbitrary machines either physical or virtual.


  Security:
    Connections are supported only by means of SSH. Particularly preferred 
    by kerberized OpenSSH.



  For extended help type:
    ${MYCALLNAME} <help-option>
    -> for a complete set of help:
       "-H all"   or "-H '*'"

    -> for help on help:
       "-H '-H'"  or "-H '-h'"

    -> for printing help as manual formatted by pr with PR_OPTS Variable:
       "-H 'print'" 

    -> for help on a specific item:
       "-H <option>[,<option>],..." 
           hyphens required for each <option>
           "-a,-b,-c".
       "-H <item>[,<item>],..."

    -> for help on development interfaces:
       "-H 'funcList|funcListMod|funcHead'"

       for specific items type

       "-H 'funcList|funcListMod|funcHead=<any-function>'"
      
END2
}


#FUNCBEG###############################################################
#NAME:
#  _printHelpEx
#
#TYPE:
#  bash-function
#
#DESCRIPTION:
#
#EXAMPLE:
#
#PARAMETERS:
#
#
#OUTPUT:
#  RETURN:
#
#  VALUES:
#
#FUNCEND###############################################################
function _printHelpEx () {
  local i=;
  [ -n "${MYHELPPATH}" ]&&for i in ${MYHELPPATH}/[0][0123456789]*[^~];do
      [ -n "$i" -a -f "$i" ]&&cat ${i};
  done
}
