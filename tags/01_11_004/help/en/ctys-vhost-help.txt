
NAME
====

ctys-vhost -  core address resolution interface

SYNTAX
======

ctys-vhost


   [-c <spent cost on execution environment>
      =MINCNT|MAXCNT|CNT
      ]
   [-C <DB sources>
      =(
        OFF
        |CLEARTMP
        |CLEARALL
        |GROUPS
        |KEEPALL
        |LIST
        |LISTCACHE
        |LISTARGETS
        |LISTGROUPS
        |MEMBERSDB
        |MACMAPONLY
        |MACMAP
        |REBUILDCACHE
      )
   [-d <debug-level>]
   [-h]
   [-H <help-options>]
   [-i <input-list>=[CTYSADDRESS|CTYS]]
   [-I <0-9>]
   [-l <USER>]
   [-M <result-set-output-reduction>
      =(FIRST|LAST|ALL|COMPLEMENT|SORT|USORT|UNIQUE)
      ]
   [-o <output-list>
      =(
         (
           ( 
             [ARCH][,]
             [CATEGORY|CAT][,]
             [CONTEXTSTRING|CSTRG][,]
             [CPORT|VNCPORT][,]
             [CTYSADDRESS|CTYS][,]
             [CTYSRELEASE][,]
             [DIST][,]
             [DISTREL][,]
             [EXECLOCATION][,]
             [GATEWAY][,]
             [HWCAP][,]
             [HWREQ][,]
             [HYPERREL|HYREL][,]
             [IDS|ID|I][,]
             [IFNAME][,]
             [LABEL|L][,]
             [MAC|M][,]
             [NETMASK][,]
             [NETNAME][,]
             [TYPE|STYPE|ST][,]
             [OS|O][,]
             [OSREL][,]
             [PLATFORM|PFORM][,]
             [PM|HOST|H][,]
             [PNAME|P][,]
             [RELAY][,]
             [RELOCCAP][,]
             [SERIALNUMBER|SERNO][,]
             [SERVERACCESS|SPORT|S][,]
             [SSHPORT][,]
             [STACKCAP|SCAP][,]
             [STACKREQ|SREQ][,]
             [TCP|T][,]
             [USERSTRING|USTRG][,]
             [UUID|U][,]
             [VCPU][,]
             [VERSION|VERNO|VER][,]
             [VMSTATE|VSTAT][,]
             [VNCBASE][,]
             [VNCDISPLAY|DISP][,]
             [VRAM][,]
           )
           [TITLE|TITLEIDX|TITLEIDXASC][,]
           [MACHINE|MAXKEY][,]
         )
         | TAB_GEN|TAB[:<tab-args>]
         | REC_GEN|REC[:<tab-args>]
         | SPEC_GEN|SPEC[:<tab-args>]
         | XML_GEN|XML[:<tab-args>]
       )
      [IP|DNS|D][,]
      [,SORT[:ALL|A|UNIQUE|U|REVERSE|R|EACH|([0-9][0-9][0-9]|[0-9][0-9]|[0-9])]
   ]
   [-p <db-directory-path-list>]
   [-r]
   [-R <runtime states>
      =[MARK|(REVERSE|R|-),]PING|SSH[,PM|VM]
      ]
   [-s]
   [-S <BasicDataManagementSupport>
     =(
        CONTENTGROUP
        |LISTALL
        |LIST
        |LISTDB
        |MEMBERSDB
        |LISTGROUP
        |MEMBERSGROUP[2345]
      )
      [-T <type-list>]
      [-V]
      [-X]
      <awk-regexpr>[ <awk-regexpr>[ <...>]]




DESCRIPTION
===========

Refer to documentation or use following option for
extended help:

     ctys -H help

     displays available online help options.

COPYRIGHT
=========

Copyright (C) 2008, 2009, 2010 Ingenieurbuero Arno-Can Uestuensoez

This is software and documentation from BASE package,

- for software see GPL3 for license conditions,
- for documents  see GFDL-1.3 with invariant sections for license conditions.

The whole document - all sections - is/are defined as invariant.

For additional information refer to enclosed Releasenotes and License files.






