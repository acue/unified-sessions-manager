#FUNCBEG###############################################################
#NAME:
#  getCurCTYSRel.sh
#
#TYPE:
#  generic-script
#
#DESCRIPTION:
#  Prints the current release of CTYS.
#
#EXAMPLE:
#
#PARAMETERS:
#
#OUTPUT:
#  RETURN:
#  VALUES:
#
#FUNCEND###############################################################

CTYS_RELEASE=01_11_019

echo -n "${CTYS_RELEASE}"
