
NAME
====

ctys-extractARPlst - generates a list of mappings

SYNTAX
======

ctys-extractARPlst


   [-d <debug-args>]
   [-E]
   [-n|-i|-m]
   [-h]
   [-H <help-options>]
   [-p <db-dir-path>]
   [-P]
   [-q]
   [-V]
   [-X]
   <ping-hostlist-in-same-segment>



DESCRIPTION
===========

Refer to documentation or use following option for
extended help:

     ctys -H help

     displays available online help options.

COPYRIGHT
=========

Copyright (C) 2008, 2009, 2010 Ingenieurbuero Arno-Can Uestuensoez

This is software and documentation from BASE package,

- for software see GPL3 for license conditions,
- for documents  see GFDL-1.3 with invariant sections for license conditions.

The whole document - all sections - is/are defined as invariant.

For additional information refer to enclosed Releasenotes and License files.






