
NAME
====

ctys-distribute - local and remote installation, bulk-distribution

SYNTAX
======

ctys-distribute


   [-d <debug-options>]
   [-D (0|1)]
   [-F (1|2|3|force|forceclean|forceall)]
   [-h]
   [-H <help-options>]
   [-l <remote login>]
   [-L <remote-login>]
   [-M <access-mask>]
   [-P (
       UserHomeCopy
      |UserHomeLinkonly
      |SharedAnyDirectory,<subopts>
      |SharedAnyLinkonly,<subopts>
      |AnyDirectory,<subopts>
      )]
   [-R <remote-host>]
   [-V]
   [-X]
   <list-of-target-acounts>






DESCRIPTION
===========

Refer to documentation or use following option for
extended help:

     ctys -H help

     displays available online help options.

COPYRIGHT
=========

Copyright (C) 2008, 2009, 2010 Ingenieurbuero Arno-Can Uestuensoez

This is software and documentation from BASE package,

- for software see GPL3 for license conditions,
- for documents  see GFDL for license conditions,

This document is created with: latex and text2tags






