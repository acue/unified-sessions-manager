#!/bin/bash

########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      01_06_001a10
#
########################################################################
#
#     Copyright (C) 2007 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
########################################################################

################################################################
#                   Begin of FrameWork                         #
################################################################

#FUNCBEG###############################################################
#
#PROJECT:
MYPROJECT="Unified Sessions Manager"
#
#NAME:
#  ctys-callVncserver
#
#AUTHOR:
AUTHOR="Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org"
#
#FULLNAME:
FULLNAME="Unified Sessions Manager"
#
#CALLFULLNAME:
CALLFULLNAME="Call VNC server"
#
#LICENCE:
LICENCE=GPL3
#
#TYPE:
#  bash-script
#
#VERSION:
VERSION=01_06_001a10
#DESCRIPTION:
#  Wrapper schript for calling vncserver.
#
#EXAMPLE:
#
#PARAMETERS:
#  $*: Pass throug new <VNC-options> and append missing defaults.
#
#OUTPUT:
#  RETURN:
#    state of call for vncserver
#  VALUES:
#    output of call of vncserver
#
#FUNCEND###############################################################

################################################################
#                     Global shell options.                    #
################################################################
shopt -s nullglob


################################################################
#       System definitions - do not change these!              #
################################################################
#Execution anchor
MYHOST=`uname -n`
MYCALLPATHNAME=$0
MYCALLNAME=`basename $MYCALLPATHNAME`

if [ -n "${MYCALLPATHNAME##/*}" ];then
    MYCALLPATHNAME=${PWD}/${MYCALLPATHNAME}
fi
MYCALLPATH=`dirname $MYCALLPATHNAME`
###################################################
#load basic library required for bootstrap        #
###################################################
MYBOOTSTRAP=${MYCALLPATH}/bootstrap
if [ ! -d "${MYBOOTSTRAP}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYBOOTSTRAP=${MYBOOTSTRAP}"
cat <<EOF  

DESCRIPTION:
  This directory contains the common mandatory bootstrap functions.
  Your installation my be erroneous.  

SOLUTION-PROPOSAL:
  First of all check your installation, because an error at this level
  might - for no reason - bypass the final tests.

  If this does not help please send a bug-report.

EOF
  exit 1
fi

MYBOOTSTRAP=${MYBOOTSTRAP}/bootstrap.01.01.002
if [ ! -f "${MYBOOTSTRAP}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYBOOTSTRAP=${MYBOOTSTRAP}"
cat <<EOF  

DESCRIPTION:
  This file contains the common mandatory bootstrap functions required
  for start-up of any shell-script within this package.

  It seems though your installation is erroneous or you detected a bug.  

SOLUTION-PROPOSAL:
  First of all check your installation, because an error at this level
  might - for no reason - bypass the final tests.

  When your installation seems to be OK, you may try to set a TEMPORARY
  symbolic link to one of the files named as "bootstrap.<highest-version>".
  
    ln -s ${MYBOOTSTRAP} bootstrap.<highest-version>

  in order to continue for now. 

  Be aware, that any installation containing the required file will replace
  the symbolic link, because as convention the common boostrap files are
  never symbolic links, thus only recognized as a temporary workaround to 
  be corrected soon.

  If this does not work you could try one of the other versions.

  Please send a bug-report.

EOF
  exit 1
fi

###################################################
#Start bootstrap now                              #
###################################################
. ${MYBOOTSTRAP}
###################################################
#OK - utilities to find components of this version#
#available now.                                   #
###################################################

#
#set real path to install, resolv symbolic links
_MYCALLPATHNAME=`bootstrapGetRealPathname ${MYCALLPATHNAME}`
MYCALLPATH=`dirname ${_MYCALLPATHNAME}`
#
###################################################
#Now find libraries might perform reliable.       #
###################################################


#current language, not really NLS
MYLANG=${MYLANG:-en}

#path for various loads: libs, help, macros, plugins
MYLIBPATH=${CTYS_LIBPATH:-`dirname $MYCALLPATH`}

###################################################
#Check master hook                                #
###################################################
bootstrapCheckInitialPath
###################################################
#OK - Now should work.                            #
###################################################

MYHELPPATH=${MYLIBPATH}/help/${MYLANG}
if [ ! -d "${MYHELPPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYHELPPATH=${MYHELPPATH}"
  exit 1
fi

MYCONFPATH=${MYLIBPATH}/conf/ctys
if [ ! -d "${MYCONFPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYCONFPATH=${MYCONFPATH}"
  exit 1
fi

if [ -f "${MYCONFPATH}/versinfo.conf" ];then
    . ${MYCONFPATH}/versinfo.conf
fi

MYMACROPATH=${MYCONFPATH}/macros
if [ ! -d "${MYMACROPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYMACROPATH=${MYMACROPATH}"
  exit 1
fi

MYPKGPATH=${MYLIBPATH}/plugins
if [ ! -d "${MYPKGPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYPKGPATH=${MYPKGPATH}"
  exit 1
fi

MYINSTALLPATH= #Value is assigned in base. Symbolic links are replaced by target


##############################################
#load basic library required for bootstrap   #
##############################################
. ${MYLIBPATH}/lib/base
. ${MYLIBPATH}/lib/libManager
#
#Germish: "Was the egg or the chicken first?"
#
#..and prevent real load order for later display.
#
bootstrapRegisterLib
baseRegisterLib
libManagerRegisterLib
##############################################
#Now the environment is armed, so let's go.  #
##############################################

if [ ! -d "${MYINSTALLPATH}" ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing:MYINSTALLPATH=${MYINSTALLPATH}"
    gotoHell ${ABORT}
fi

MYOPTSFILES=${MYOPTSFILES:-$MYLIBPATH/help/$MYLANG/*_base_options} 
checkFileListElements "${MYOPTSFILES}"
if [ $? -ne 0 ];then
    ABORT=1;
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing:MYOPTSFILES=${MYOPTSFILES}"
    gotoHell ${ABORT}
fi


################################################################
# Main supported runtime environments                          #
################################################################
#release
TARGET_OS="Linux-CentOS/RHEL(5+)"

#to be tested - coming soon
TARGET_OS_SOON="OpenBSD+Linux(might work now):Ubuntu+OpenSuSE"

#to be tested - might be almsot OK - but for now FFS
#...probably some difficulties with desktop-switching only?!
TARGET_OS_FFS="FreeBSD+Solaris/SPARC/x86"

#release
TARGET_WM="Gnome"

#to be tested - coming soon
TARGET_WM_SOON="fvwm"

#to be tested - coming soon
TARGET_WM_FORESEEN="KDE(might work now)"

################################################################
#                     End of FrameWork                         #
################################################################

#
#Perform the initialization according and based on LD_PLUGIN_PATH.
#
MYROOTHOOK=${MYINSTALLPATH}/plugins/hook
if [ ! -f "${MYROOTHOOK}" ];then 
    ABORT=2
    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing packages hook: hook=${MYROOTHOOK}"
    gotoHell ${ABORT}
fi
. ${MYROOTHOOK}
#initPackages "${MYROOTHOOK}"

hookPackage "${MYLIBPATH}/plugins/CORE/CACHE"


#
#Verify OS support
#
case ${MYOS} in
    Linux)
	[ -z "$X11XAUTH" ]&&X11AUTH=`getPathName $LINENO $BASH_SOURCE ERROR xauth /usr/bin`
	;;
    OpenBSD)
	[ -z "$X11XAUTH" ]&&X11AUTH=`getPathName $LINENO $BASH_SOURCE ERROR xauth /usr/X11R6/bin`
	;;
    SunOS)
	[ -z "$X11XAUTH" ]&&X11AUTH=`getPathName $LINENO $BASH_SOURCE ERROR xauth /usr/openwin/bin`
	;;
    *)
        printINFO 1 $LINENO $BASH_SOURCE 1 "${MYCALLNAME} is not supported on ${MYOS}"
	gotoHell 0
	;;
esac



if [ "${*}" != "${*//-X/}" ];then
    C_TERSE=1
fi
if [ "${*}" != "${*//-V/}" ];then
    if [ -n "${C_TERSE}" ];then
	echo -n ${VERSION}
    else
	echo "$0: VERSION=${VERSION}"
    fi
    exit 0
fi

#Source pre-set environment from user
if [ -f "${HOME}/.ctys/ctys.conf" ];then
  . "${HOME}/.ctys/ctys.conf"
fi

#Source pre-set environment from installation 
if [ -f "${MYCONFPATH}/ctys.conf" ];then
  . "${MYCONFPATH}/ctys.conf"
fi

#system tools
if [ -f "${HOME}/.ctys/systools.conf.${MYDIST}" ];then
    . "${HOME}/.ctys/systools.conf.${MYDIST}"
fi

if [ -f "${MYCONFPATH}/systools.conf.${MYDIST}" ];then
    . "${MYCONFPATH}/systools.conf.${MYDIST}"
else
    if [ -f "${MYCALLPATH}/../conf/ctys/systools.conf.${MYDIST}" ];then
	. "${MYCALLPATH}/../conf/ctys/systools.conf.${MYDIST}"
    else
	ABORT=1;
	printERR $LINENO $BASH_SOURCE ${ABORT} "Missing system tools configuration file:\"systools.conf.${MYDIST}\""
	printERR $LINENO $BASH_SOURCE ${ABORT} "Check your installation."
	gotoHell ${ABORT}
    fi
fi

#Source pre-set environment from user
if [ -f "${HOME}/.ctys/vnc/vnc.conf.${MYOS}" ];then
  . "${HOME}/.ctys/vnc/vnc.conf.${MYOS}"
fi

#Source pre-set environment from user
if [ -f "${MYCONFPATH}/vnc/vnc.conf.${MYOS}" ];then
  . "${MYCONFPATH}/vnc/vnc.conf.${MYOS}"
fi


################################################################
#    Specific definitions - User-Customizable  from shell      #
################################################################

case `getCurOS` in
    Linux)
	if [ ! -f $HOME/.vnc/passwd ];then
	    cp $HOME/.vnc/passwd.realVNC $HOME/.vnc/passwd
	fi
	;;
    OpenBSD)
	if [ ! -f $HOME/.vnc/passwd ];then
	    cp $HOME/.vnc/passwd.tightVNC $HOME/.vnc/passwd
	fi
	;;
    SunOS)
	if [ ! -f $HOME/.vnc/passwd ];then
	    cp $HOME/.vnc/passwd.tightVNC $HOME/.vnc/passwd
	fi
	;;
esac


. ${MYLIBPATH}/lib/cli/cli

_myJob=`cliGetOptValue "-j" ${@}`
printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "_myJob=$_myJob"

#Used only for current script, to be removed for vncviewer
CLISERVERONLY="-d -j "


#
#Defaults fo vncserver.
#  +kb ?
#CLIDEFAULTS=" -name ${NAME:-VncServerDefault} -depth 16 -localhost -nolisten tcp "
CLIDEFAULTS=" -name ${NAME:-VncServerDefault} -depth 16  "

printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "DISPLAY=$DISPLAY"

printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "\$@=$@"
X="`cliOptionsStrip REMOVE ${CLISERVERONLY} -- ${@}`"
printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "\$X=$X"
X="`cliOptionsAddMissing ${CLIDEFAULTS} -- ${X}`"
printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "\$X=$X"

LBL=`echo " $X "|sed -n 's/.*-name *\([^ ]*\) *.*/\1/p'`
printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "LBL=${LBL}"

printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "PATH=${PATH}"
OUTPUT="${VNCSERVER_NATIVE} ${X}"
printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "CALL=${OUTPUT}"

#generic: Linux+OBSD+Solaris+..
RESULT=`${OUTPUT} ${X} 2>&1|awk -F':' -v d=${LBL} '/d/&&!/Log file/&&/^New/{id=$2;gsub(" .*$","",id);printf("%s\n",id);}'`
RESULT="${RESULT%% *}"
printDBG $S_BIN ${D_MAINT} $LINENO $BASH_SOURCE "RESULT=${RESULT}"
echo $RESULT
