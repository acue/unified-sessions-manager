TARGET_OS        = Linux: CentOS/RHEL, Fedora, SuSE/openSUSE, debian, Ubuntu 
                   BSD:   OpenBSD
TARGET_VM        = VMware, Xen, QEMU
TARGET_WM        = X11, Gnome, fvwm
GUEST_OS         = ANY(some with limited native-acces support)
