#!/bin/bash

########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      01_06_001a02
#
########################################################################
#
#     Copyright (C) 2008 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
########################################################################


#@#MAGICID-IGNORE


################################################################
#                     Global shell options.                    #
################################################################
shopt -s nullglob

#
#The root! 
#
#Require this, because this script could be almost located anywhere,
#and the UnifiedSesessionsManager could be installled anywhere else,
#thus all is floating by definition, so give me a hook please!
#
if [ -z "${CTYS_LIBPATH}" ];then
    echo "ERROR:This script requires the variable CTYS_LIBPATH to be set.">&2
    echo "ERROR:Refer to \"UnifiedSessionsManager\" user manual.">&2
    exit 1;
fi
MYLIBPATH=${CTYS_LIBPATH}


################################################################
#       System definitions - do not change these!              #
################################################################
#Execution anchor
MYCALLPATHNAME=$0
MYCALLNAME=`basename $MYCALLPATHNAME`

if [ -n "${MYCALLPATHNAME##/*}" ];then
    MYCALLPATHNAME=${PWD}/${MYCALLPATHNAME}
fi
MYCALLPATH=`dirname $MYCALLPATHNAME`


###################################################
#load basic library required for bootstrap        #
###################################################
MYBOOTSTRAP=${MYLIBPATH}/bin/bootstrap
if [ ! -d "${MYLIBPATH}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYBOOTSTRAP=${MYBOOTSTRAP}"
cat <<EOF  

DESCRIPTION:
  This directory contains the common mandatory bootstrap functions.
  Your installation my be erroneous.  

SOLUTION-PROPOSAL:
  First of all check your installation, because an error at this level
  might - for no reason - bypass the final tests.

  If this does not help please send a bug-report.

EOF
  exit 1
fi

MYBOOTSTRAP=${MYBOOTSTRAP}/bootstrap.01.01.001
if [ ! -f "${MYBOOTSTRAP}" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing:MYBOOTSTRAP=${MYBOOTSTRAP}"
cat <<EOF  

DESCRIPTION:
  This file contains the common mandatory bootstrap functions required
  for start-up of any shell-script within this package.

  It seems though your installation is erroneous or you detected a bug.  

SOLUTION-PROPOSAL:
  First of all check your installation, because an error at this level
  might - for no reason - bypass the final tests.

  When your installation seems to be OK, you may try to set a TEMPORARY
  symbolic link to one of the files named as "bootstrap.<highest-version>".
  
    ln -s ${MYBOOTSTRAP} bootstrap.<highest-version>

  in order to continue for now. 

  Be aware, that any installation containing the required file will replace
  the symbolic link, because as convention the common boostrap files are
  never symbolic links, thus only recognized as a temporary workaround to 
  be corrected soon.

  If this does not work you could try one of the other versions.

  Please send a bug-report.

EOF
  exit 1
fi

###################################################
#Start bootstrap now                              #
###################################################
. ${MYBOOTSTRAP}
###################################################
#OK - utilities to find components of this version#
#are available now.                               #
###################################################

#
#set real path to install, resolv symbolic links
_MYCALLPATHNAME=`bootstrapGetRealPathname ${MYCALLPATHNAME}`
MYCALLPATH=`dirname ${_MYCALLPATHNAME}`
#
###################################################
#Now find libraries might perform reliable.       #
###################################################


#current language, not really NLS
MYLANG=${MYLANG:-en}

#path for various loads: libs, help, macros, plugins
MYLIBPATH=${CTYS_LIBPATH:-`dirname $MYCALLPATH`}

###################################################
#Check master hook                                #
###################################################
bootstrapCheckInitialPath
###################################################
#OK - Now should work.                            #
###################################################


##############################################
#load basic library required for bootstrap   #
##############################################
if [ ! -f "${MYLIBPATH}/lib/base" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing mandatory initial library:${MYLIBPATH}/lib/base"
  exit 1
fi
. ${MYLIBPATH}/lib/base


if [ ! -f "${MYLIBPATH}/lib/libManager" ];then
  echo "${MYCALLNAME}:$LINENO:ERROR:Missing mandatory initial library:${MYLIBPATH}/lib/libManager"
  exit 1
fi
. ${MYLIBPATH}/lib/libManager

#
#"Was the egg or the chicken first?"
#
#..and prevent real load order for later display.
#
bootstrapRegisterLib
baseRegisterLib
libManagerRegisterLib
##############################################
#Now the environment is armed, so let's go.  #
##############################################


printDBG $S_QEMU ${D_FLOW} $LINENO $BASH_SOURCE "LOADED:MYLIBPATH=${MYLIBPATH}"

########################################################################
#
#Load shared environment of QEMU plugin.
#
########################################################################

if [ -d "${HOME}/.ctys" -a -d "${HOME}/.ctys/qemu" ];then
    if [ -f "${HOME}/.ctys/qemu/qemu.conf.${MYOS}" ];then
	. "${HOME}/.ctys/qemu/qemu.conf.${MYOS}"
    fi
fi

if [ -d "${MYCONFPATH}/qemu" ];then
    if [ -f "${MYCONFPATH}/qemu/qemu.conf.${MYOS}" ];then
	. "${MYCONFPATH}/qemu/qemu.conf.${MYOS}"
    fi
fi



########################################################################
#
#This wrapper if to be applied to the "coldfire-test-0.1" example of
#QEMU distribution.
#
#This example has in current version the particular advantage, that
#it provides a pre-configured DHCPclient within the guestOS.
#
#The following CLI parameters are supported by this script:
#
#  
#  $1:CONSOLE
#       EMACS, GTERM, XTERM: 
#            Mapped to CLI as a output terminal, should be done in 
#            frontend.
#       CLI: To be used in a terminal
#       SDL: To be used for X11 GUI.
#       VNC: To be used for VNC, this is the only mode which supports 
#            CONNECTIONFORWARDING too.
#       NONE:None.
#
#       DDD, ECLIPSE, GDB, VNC, VMW:
#            Not supported.
#
#  $2:BOOTMODE
#       VHDD:Default and only supported mode for this image.
#            
#       CD, ISO, KERNEL, PXE:
#            Not supported.
#
#  $3:<call-args>
#  $4:<x-args>
#
#
########################################################################

CONSOLE=$1;shift
BOOTMODE=$1;shift

ARGSADD=$*


#@#MAGICID-IGNORE
#
#import configuration keys
#
myKeys="${WRAPPERPATH}/${MYCALLNAME/.ctys/.conf}"
if [ -e "${myKeys}" ];then
    printDBG $S_QEMU ${D_FLOW} $LINENO $BASH_SOURCE "myKeys=${myKeys}"
    . "${myKeys}"
fi

#
#Start assembly of call.
#

#
#The socket for access to VDE's virtual switch port: vde_switch
#should be already defined in the shared configuration files.
QEMUSOCK=${QEMUSOCK:-/var/tmp/vde_switch0.$USER}

#
#inherited by export, but normally as defined here
#should be already defined in the shared configuration files.
QEMUBIOS=${QEMUBIOS:-$HOME/qemu/pc-bios}

#
#ATTENTION:
#  Full path required for ps-based LIST, so do not 
#  change this, but the device as required.
LASTPATH=" -kernel ${MYCALLPATH}/vmlinux-2.6.21-uc0 "

#
#inherited by export, but normally as defined here
QEMU=${QEMU:-vdeq}

QCALL="${QEMUCALL} ${QEMU} ";
QCALL="${QCALL} qemu-system-m68k "


QCALL="${QCALL} -m 256 "
QCALL="${QCALL} -localtime "
QCALL="${QCALL} -k de "
QCALL="${QCALL} -L ${QEMUBIOS} "
QCALL="${QCALL} -net nic,macaddr=${MAC0} "
QCALL="${QCALL} -net vde,sock=${QEMUSOCK} "


case ${BOOTMODE} in
    VHDD);;
    *)
      ABORT=127
      printERR $LINENO $BASH_SOURCE ${ABORT} "Not supported for qemu-system-m68k: BOOTMODE=\"${BOOTMODE}\""
      gotoHell ${ABORT}
    ;;
esac


case ${CONSOLE} in
    CLI)QCALL="${QCALL} -nographic  ${ARGSADD} ${LASTPATH}";;
    *)
      ABORT=127
      printERR $LINENO $BASH_SOURCE ${ABORT} "Unknown display CONSOLE=\"${CONSOLE}\""
      gotoHell ${ABORT}
    ;;
esac

printDBG $S_QEMU ${D_FLOW} $LINENO $BASH_SOURCE "${QCALL}"
callErrOutWrapper $LINENO $BASH_SOURCE  ${QEMUCALL} "${QCALL}"
gotoHell 0

