#!/bin/bash

########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      01_06_001a09
#
########################################################################
#
# Copyright (C) 2007 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
########################################################################

_myPKGNAME_VMW_ENUMERATE="${BASH_SOURCE}"
_myPKGVERS_VMW_ENUMERATE="01.06.001a09"
hookInfoAdd $_myPKGNAME_VMW_ENUMERATE $_myPKGVERS_VMW_ENUMERATE
_myPKGBASE_VMW_ENUMERATE="`dirname ${_myPKGNAME_VMW_ENUMERATE}`"


#FUNCBEG###############################################################
#NAME:
#  enumerateMySessionsVMW
#
#TYPE:
#  bash-function
#
#DESCRIPTION:
#  Enumerates all VMW sessions, therefore the vmx-files will be scanned
#  and the matched attributes displayed.
#
#  Therefore the following order of files will be scanned for values:
#
#    1. <pname>
#       The standard configuration file for VM, as given.
#
#    2. <pname-prefix>.ctys
#       The prefix of given filename with the ".ctys" suffix.
#
#    3. <pname-dirname>.ctys
#       The dirname of given file with ".ctys" suffix.
#
#  In each case the searched key is expected to have the prefix "#@#" 
#  within the file.
#
#  For detailed interface description refer to genric dispatcher.
#
#EXAMPLE:
#
#PARAMETERS:
#  $1: <basename-list>=<basename>[%<basename-list>]
#
#GLOBALS:
#OUTPUT:
#  RETURN:
#
#  VALUES:
#    standard enumerate output format
#    
#
#FUNCEND###############################################################
function enumerateMySessionsVMW () {
    printDBG $S_VMW ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:\$@=${@}"

    local _myMAC=;
    local _myMAClst=;
    local _myIP=;
    local _myIPlst=;

    local _base="${*//\%/ }"
    _base=${_base:-$HOME $RS_PREFIX_R $RS_PREFIX_L}
    printDBG $S_VMW ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:_base=${_base}"

    #just for safety ...
    if [ -z "${_base}" ];then
 	ABORT=1
 	printERR $LINENO $BASH_SOURCE ${ABORT} "Missing base for search: Check your path/file call-parameters.";
	gotoHell ${ABORT}
    fi



    #This value is defined as anchor to be supported from VMs config file!!!
    function getMAClst_VMW () {
        #might be cached already, don't forget to reset for each record!!!
        if [ -n "${_myMAC}" ];then
	    echo ${_myMAC}
	    return
        fi
	local _IP=;
	for i in `getConfFilesList "${1}"`;do
	    _IP=`sed -n 's/\t//g;/^#/d;s/ethernet\([0-9]*\).address *= *"\([^"]*\)"/\1=\2/p' "${i}"`;
            [ "$_IP" != "" ]&&break;
	done
 	echo $_IP
    }


    function getUUID () {
	local _IP=;
	if [ -z "${1}" ];then
	    gwhich dmidecode 2>/dev/null >/dev/null
	    if [ $? == 0 ];then
		_IP=`dmidecode |awk '/UUID/{if(NF==2)print $2}'|sed 's/-//g'`
	    fi
	else
	    for i in `getConfFilesList "${1}"`;do
		_IP=`sed -n '/uuid.bios/!d;s/[ -]//g;p' "${i}"|getConfValueOf "uuid.bios"`
  		[ "$_IP" != "" ]&&break;
	    done
	fi
	echo ${_IP##* }
    }

    function getLABELVMW () {
	local _IP=;
	for i in `getConfFilesList "${1}"`;do
	    _IP=`cat  "${i}"|getConfValueOf "displayName"`
  	    [ "$_IP" != "" ]&&break;
	    _IP=`cat  "${i}"|getConfValueOf "#@#LABEL"`
            [ "$_IP" != "" ]&&break;
	done
	echo ${_IP##* }
    }

    function getOSVMW () {
	local _IP=;
	for i in `getConfFilesList "${1}"`;do
	    _IP=`sed -n 's/^[^#]*guestOS *= *"\([^"]*\)"/\1/p' "${i}"|\
                   awk '{if(x){printf(" %s",$0);}else{printf("%s",$0);}x=1;}'`;
  	    [ "$_IP" != "" ]&&break;
	done
	echo ${_IP##* }
    }

    function getVNCACCESSPORT_VMW () {
	local _IP=;
	for i in `getConfFilesList "${1}"`;do
 	    _IP=`cat "${i}"|getConfValueOf "RemoteDisplay.vnc.port"`
            if [ "$_IP" != "" ];then
		printDBG $S_CORE ${D_FRAME} $LINENO $BASH_SOURCE "$FUNCNAME:${_IP} from ${i}"
		break;
	    fi
	done
	if [ -n "${_IP// /}" ];then
	    printDBG $S_VMW ${D_MAINT} $LINENO $BASH_SOURCE "$FUNCNAME:_IP=${_IP} from ${1}"
	    echo ${_IP##* }
            return
	fi
	getVNCACCESSPORT "${1}"
    }


    function getGUESTVRAM_VMW () {
	local _IP=;
	for i in `getConfFilesList "${1}"`;do
	    _IP=`cat  "${i}"|getConfValueOf "memsize"`
            if [ "$_IP" != "" ];then
		printDBG $S_CORE ${D_FRAME} $LINENO $BASH_SOURCE "$FUNCNAME:${_IP} from ${i}"
		break;
	    fi
	done
	if [ -n "${_IP// /}" ];then
	    printDBG $S_VMW ${D_MAINT} $LINENO $BASH_SOURCE "$FUNCNAME:_IP=${_IP} from ${1}"
	    echo ${_IP##* }
            return
	fi
	getGUESTVRAM "${1}"
    }


    #ID/PNAME should be shown as unique and unambiguos ID, ready to be used
    #Make each base absolut, if not yet
    local _i2=;
    local _baseabs=;
    for _i2 in ${_base};do
	_i2="${_i2//\~/$HOME}"
	if [ "${_i2#/}" == "${_i2}" ];then
	    _baseabs="${_baseabs} ${PWD}/${_i2}"
	else
	    _baseabs="${_baseabs} ${_i2}"
	fi
	local X=;
	printDBG $S_VMW ${D_MAINT} $LINENO $BASH_SOURCE "$FUNCNAME:_baseabs=${_baseabs}"
	if [ ! -f "${_myPKGBASE_VMW_ENUMERATE}/enumfilter.awk" ];then
 	    ABORT=1
 	    printERR $LINENO $BASH_SOURCE ${ABORT} "Missing:${_myPKGBASE_VMW_ENUMERATE}/enumfilter.awk";
	    gotoHell ${ABORT}
	fi
	printDBG $S_VMW ${D_BULK} $LINENO $BASH_SOURCE "$FUNCNAME:Use MATCH-FILTER:${_myPKGBASE_VMW_ENUMERATE}/enumfilter.awk";

    done
    checkPathElements FIND-PATH ${_baseabs// /:}
    {
	find ${_baseabs} -name '*.vmx' \
            -exec awk -F'=' -v matchMin=5 -f ${_myPKGBASE_VMW_ENUMERATE}/enumfilter.awk {} \; \
            -print
    }|\
    while read X;do
        CURRENTENUM=${X};
        local _out=;
    	printDBG $S_VMW ${D_UID} $LINENO $BASH_SOURCE "$FUNCNAME:MATCH=${X}"
 	if [ -f "${X}" ];then
	    if [ ! -r "${X}" ];then
		_out="${MYHOST}";
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out}VMW";
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";  
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		_out="${_out};";
		echo ${_out}
	    else
                local _out1=;
		_out1="${MYHOST}"; 
		_out1="${_out1};`getLABELVMW ${X}`";
		_out1="${_out1};${X}";
		_out1="${_out1};`getUUID ${X}`";
		#_out="${_out};`getMAC ${X}`";
                local _out2=;
		_out2="${_out2};";
		_out2="${_out2};`getVNCACCESSPORT_VMW  ${X}`";
		_out2="${_out2};";
		_out2="${_out2};";
		#_out="${_out};`getIP ${X}`";
                local _out3=;
		_out3="${_out3};VMW";
		_out3="${_out3};`getDIST ${X}`";
		_out3="${_out3};`getDISTREL ${X}`";

                local _ostrial=`getOS ${X}`;
                if [ -z "$_ostrial" ];then
                    #even though might be something like "other"
		    _ostrial="`getOSVMW ${X}`";
		fi
		_out3="${_out3};${_ostrial}";

		_out3="${_out3};`getOSREL ${X}`";
		_out3="${_out3};`getVERNO ${X}`";
		_out3="${_out3};`getSERNO ${X}`";
		_out3="${_out3};`getCATEGORY ${X}`";
		_out3="${_out3};`getVMSTATE ${X}`";
		_out3="${_out3};`getHYPERREL ${X}`";
		_out3="${_out3};`getSTACKCAP ${X}`";
		_out3="${_out3};`getSTACKREQ ${X}`";
		_out3="${_out3};`getHWCAP ${X}`";
		_out3="${_out3};`getHWREQ ${X}`";
		_out3="${_out3};`getEXECLOCATION ${X}`";
		_out3="${_out3};`getRELOCCAP ${X}`";
		#_out3="${_out3};`getSSHPORT  ${X}`";
                local _out4=;
		#NETNAME:_out4="${_out4};";
		_out4="${_out4};";
		_out4="${_out4};";
		_out4="${_out4};";
		_out4="${_out4};";
                 #_out="${_out};`getGUESTIFNAME ${X}`";
                local _out5=;
		_out5="${_out5};`getCTYSRELEASE ${X}`";
                 #_out="${_out};`getGUESTNETMASK ${X}`";
                local _out6=;
		#_out6="${_out6};`getGATEWAY ${X}`";
		#_out5="${_out5};`getRELAY ${X}`";
                local _out7=;
		_out7="${_out7};`getGUESTARCH ${X}`";
		_out7="${_out7};`getGUESTPLATFORM ${X}`";
		_out7="${_out7};`getGUESTVRAM_VMW ${X}`";
		_out7="${_out7};`getGUESTVCPU ${X}`";
		_out7="${_out7};`getCONTEXTSTRING ${X}`";
		_out7="${_out7};`getUSERSTRING ${X}`";

		local i=0;
		_myMAC=`getMAClst_VMW ${X}`;
		printDBG $S_VMW ${D_MAINT} $LINENO $BASH_SOURCE "$FUNCNAME:_myMAC=${_myMAC}";

		_myIP=`getIPlst ${X}`;
		printDBG $S_VMW ${D_MAINT} $LINENO $BASH_SOURCE "$FUNCNAME:_myIP=${_myIP}";

		local _myIFlst=`getIFlst INTERFACES "${_myMAC}" "${_myIP}"`
		local i3=;
		local _out=;

		printDBG $S_VMW ${D_MAINT} $LINENO $BASH_SOURCE "$FUNCNAME:_myIFlst=<${_myIFlst}>";
		if [ -n "${_myIFlst// /}" ];then
		    for i3 in $_myIFlst;do
			_out="${_out1};${i3%=*}${_out2}";
			i3=${i3#*=};

			local A[0]=${i3%%\%*};i3=${i3#*\%};
			A[1]=${i3%%\%*};i3=${i3#*\%};
			A[2]=${i3%%\%*};i3=${i3#*\%};
			A[3]=${i3%%\%*};i3=${i3#*\%};
			A[4]=${i3%%\%*};i3=${i3#*\%};
			A[5]=${i3%%\%*};

			local _myNetName=`netGetNetName "${A[0]}"`
			_out="${_out};${A[0]}${_out3};${A[4]};${_myNetName}${_out4};${A[3]}${_out5};${A[1]};${A[5]};${A[2]}${_out7}"
			printDBG $S_VMW ${D_MAINT} $LINENO $BASH_SOURCE "$FUNCNAME:_out=<${_out}>";
			echo -e "${_out}"
		    done
		else
		    _out="${_out1};${_out2};${_out3};;${_out4};${_out5};;;${_out7}"
		    printDBG $S_VMW ${D_MAINT} $LINENO $BASH_SOURCE "$FUNCNAME:_out=<${_out}>";
		    echo -e "${_out}"
		fi
		_myMAC=; #reset cache
		_myIP=;  #reset cache
	    fi
	fi
    done
}



