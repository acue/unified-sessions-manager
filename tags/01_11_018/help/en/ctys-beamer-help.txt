
NAME
====

ctys-beamer - transfers the final execution to a remote host

SYNTAX
======

ctys-beamer



   [-b <background-options>]
   [--ctys-predetach-holdtime=<timeout-secs>]
   [-d <debug-options>]
   [--display-only]   
   [--getfreeport}
   [-h]
   [-H <help-options>]
   [-L <remote-login>]
   [--mode=(
        (CTYS-HOPS|CH|0)
       |(SSH-CHAIN|SC|1)
       )
   ]
   [-R <remote-host-list>]
   [--ssh-hop-holdtime=<holdtime>]
   [--ssh-tunnel-holdtime=<holdtime>]
   [-V]
   [--x11]
   [-X]
   [<bypassed-ctys-options>]
   (--|--beam-this)
   <remote-command>


   <remote-host-list>:=<relay-chain>[,<remote-host-list>]

   <relay-chain>:=<relay-host>[%<relay-chain>]

   <relay-host>:=[<user>@](<host>|<access-point>)

   <access-point>:=<physical-access-point>|<virtual-access-point>

   <physical-access-point>:=<machine-address>
   <virtual-access-point>:=<machine-address>


   Evaluated options of bypassed standard set:
     <bypassed-ctys-options>:=[
       -b (0|1|2|3)[,(0|1|2|3)]
     ]



DESCRIPTION
===========

Refer to documentation or use following option for
extended help:

     ctys -H help

     displays available online help options.

COPYRIGHT
=========

Copyright (C) 2008, 2009, 2010, 2011 Ingenieurbuero Arno-Can Uestuensoez

For BASE package following licenses apply,

- for software see GPL3 for license conditions,
- for documents see GFDL-1.3 with invariant sections for license conditions,

This document is part of the DOC package,

- for documents and contents from DOC package see 

  'Creative-Common-Licence-3.0 - Attrib: Non-Commercial, Non-Deriv'

  with optional extensions for license conditions.

For additional information refer to enclosed Releasenotes and License files.






