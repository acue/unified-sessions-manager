#  -*- mode: python; -*-
########################################################################
#
#PROJECT:      Unified Sessions Manager
#AUTHOR:       Arno-Can Uestuensoez - acue@UnifiedSessionsManager.org
#MAINTAINER:   Arno-Can Uestuensoez - acue_sf1@sourceforge.net
#SHORT:        ctys
#CALLFULLNAME: Commutate To Your Session
#LICENCE:      GPL3
#VERSION:      MARKER_VERNO
#
########################################################################
#
#     Copyright (C) 2010 Arno-Can Uestuensoez (UnifiedSessionsManager.org)
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
########################################################################



#***********************************************************************
#                        Don't activate                                *
#***********************************************************************
#@#MAGICID-IGNORE
#***********************************************************************

#
#INSTALLER-INFO:
#===============
#
#@#INST_EDITOR               =  MARKER_EDITOR
#@#INST_DATE                 =  MARKER_DATE
#@#INST_CTYSREL              =  MARKER_CTYSREL
#@#INST_VERNO                =  MARKER_VERNO
#@#INST_SERNO                =  MARKER_SERNO
#@#INST_UID                  =  MARKER_UID
#@#INST_GID                  =  MARKER_GID
#@#INST_HOST                 =  MARKER_HOST
#@#INST_ACCELERATOR          =  MARKER_ACCELERATOR

#***********************************************************************



########################################################################
#
#
#
#REMARK:
#  For now just a loose integration into the Xen-Runtime-Framework is 
#  implemented. The actual work is done within the UnifiedSessionsManager
#  framework.
#
#  Particularly ease of customization of this configuration for 'anybody' 
#  is the intention, thus avoiding enhanced Python features for now, and
#  giving almost literally a 'workFLOW'.
#
#  Thus not using 'xm_vars' for now!
#
#
#  The required skills profile is:
#  -> bash knowledge
#  -> + basic understanding of programming a control-flow
#  -> + an idea what the following statements could mean, it is
#       what they look like.
#
#
#  Required imports:
#
#  import statement 'os' 
#     The import statement 'os' is the OS-interface.
#     Check the interface by calling:
#        python
#        import os
#        dir(os)
#    
#  import statement 're'
#     The import statement 're' is the regular expressions-interface.
#     Check the interface by calling:
#        python
#        import re
#        dir(re)
#
#  Refer to additional Python documentation.
#
#
#
#
########################################################################



########################################################################
#
#
#DESCRIPTION:
#
#  Generic configuration for HVM and PARA  virtualization.
#
#  Be aware that HVM and PARA  - once defined and used by installing a system -
#  are no longer exchangeable without a fresh installation replacing the current!
#  This might change - and probably is in specific cases possible. 
#  Anyhow, not generally comitted!
#
#  Some effort is spent in order to avoid usage of kpartx, thus modification might
#  be somewhat easier for the non-admins. This is particularly forseen for 
#  debootstrap with multiple partitions/slices.
#
#  Use with 'xm' for 
#  -> installation:   xm ... install='y'
#  -> operations:     xm ... [install=''] 
#
#  -> console:        xm ... con=(sdl|vnc|nographic|textonly)
#                            default:sdl
#                            sdl
#                              synchronous graphics, state is 
#                              coupled to VM
#
#                            vnc
#                              asynchronous graphics, state of
#                              VM is independend
#
#                            nographic
#                              No graphic, but with an additional
#                              vnc server running
#
#                            textonly
#                              No graphic, and no additional 
#                              vnc server
#
#
#
#  -> trace:          xm .... ctystrace=1
#
#
########################################################################



import os, re


#
# define install else operations mode
#
try:
    _inst=install
except:
    install='off'

try:
    _x=ctystrace
except:
    ctystrace='0'

mytrace=ctystrace

if mytrace == '1':
    print 'mytrace is on'



#
# quick hack due to lack of interface knowledge
# argv-position is currently defined to be fixed
#
# defined as a global knowledge of actual posiotion
#
myPosition=os.path.dirname(sys.argv[2])
if myPosition =='':
    myPosition=os.getcwd()

if mytrace == '1':
    print 'mytrace: myPosition=',myPosition



#
# The 'kernelbase' is used to make the installation of a 
# paravirtualized VM with an external DomU kernel platform/version
# indendent. For now the modules are saved just within the VM, 
# whereas the kernel and initrd are stored to '<kernelbase>/boot'.
# For additional information refer to the '--save-kernel'
# parameter of the 'ctys-createConfVM' utility.
#
#
# REMARK:
#  currently the kernel seems to use the modules contained within 
#  the VM(what is not yet veriried), anyhow, has to be checked.
#
kernelbase=myPosition+'/boot'
if not os.path.exists(kernelbase):
    kernelbase=''

if mytrace == '1':
    print 'mytrace: kernelbase=',kernelbase





#
#============================================================
#MISC
#============================================================
#
name = "MARKER_LABEL"
uuid="MARKER_UUID"
vcpus = MARKER_SMP
memory = MARKER_MEMSIZE
localtime = MARKER_LOCALTIME





#
#============================================================
#ARCH
#============================================================
#
arch = os.uname()[4]
if re.search('64$', arch):
    arch_libdir = 'lib64'
else:
    arch_libdir = 'lib'

pae=1
apic=1
acpi=1

#
# The builder should not be changed once the DomU is installed,
# anyhow for experiments it's open by your own responsibility.
# In no case success is granted! 
try:
    _builder=builder
except:
    _builder='MARKER_ACCELERATOR'
    if _builder == 'HVM':
        _builder='hvm'
        builder='hvm'
    elif _builder != 'hvm':
        _builder=''



if _builder == 'hvm':
    #
    # allocate device_model
    #
    try:
        # "predefinition should now" what it does!
        _dm=device_model
    except:
        #
        # CentOS and any other upstream derivative, opensuse
        device_model = '/usr/' + arch_libdir + '/xen/bin/qemu-dm'
        if not os.path.exists(device_model):
            #
            # debian and derivatives
            device_model = '/usr/' + arch_libdir + '/xen-default/bin/qemu-dm'
            if not os.path.exists(device_model):
                #
                # for now unexpected!
                raise "Unsupported distribution, missing device_model:'qemu-dm'!"

    if mytrace == '1':
        print "mytrace:INFO:device_model='"+device_model+"'"

    #
    # device backend
    _backend='ioemu:'
    _backendnet='ioemu'

else:
    _backend=''
    _backendnet=''




#
#============================================================
#HDD
#============================================================
#
# For now the only install and boot-media is hdd.
#
# Assume coallocation of hdd-image in one directory as default.
# The current convention is to use 'xvda'.
#
# The basic assumption for generated VMs is to have one
# system disk. When more are required this has to be 
# configured manually.
#
# Therefore the following flow just presumes the first
# HDD to be mandatory, the remianing are seen as optional.
#
# The primary intention is to avoid sub-partioning by offline
# tools such as kpartx is appropriate in case of debootsrap,
# but will raise the required skills for the users and tool-development
# unnecessarily.
#
# The 'online-installing' OSs handle the disk management by their 
# full scope of utilities the user is expected to be familiar with.
#
_diskList=[]
def addHDD (_hdPath, _devName, _driver, _opt ):
    #just a hack
    global myPosition,os,mytrace,_diskList,_backend

    if _hdPath == '':
        if _opt == 1:
            return
        else:
            raise "Erroneous input:_hdPath"

    if _devName == '':
        raise "Erroneous input:_devName"

    if _hdPath[0] != '/':
        _hdPath=myPosition+'/'+_hdPath

    if os.path.exists(_hdPath):
        if mytrace == '1':
            print "mytrace:INFO:Use hd='"+_hdPath+"'"
        _hd=_driver+':'+_hdPath+','+_backend+_devName+',w'
    else:
        if _opt == 1:
            return
        else:
            print 
            print "ERROR:Current version pre-requires a prepared image."
            print "ERROR:Missing "+_hdPath
            print 
            raise "ERROR:Missing hd, not supported:hd='"+_hdPath+"'"

    _diskList+=[ _hd, ]

    if mytrace == '1':
        print 'mytrace: _diskList=',_diskList



#
# expects one HDD at least, setting the root device for the DomU-kernel
# as well, refer to the trailer at the end of this file
try:
    _hd=hd0
except:
    _hd='MARKER_HDD_A'

_hd_dev='MARKER_HDD_a'

#
# some drivers
#
_driver='file'
#_driver='tap:aio'

addHDD (_hd, _hd_dev, _driver, 0 )
addHDD ('MARKER_HDD_B', 'MARKER_HDD_b', _driver, 1 )
#C is reserved for CD/DVD: MARKER_HDD_c:cdrom
addHDD ('MARKER_HDD_D', 'MARKER_HDD_d', _driver, 1 )






#
#============================================================
#CD/DVD-0 - instcdrom
#============================================================
#
# For installation onyl, thus describes the system, though is 'burned-in' fixed.
#
# Adapt this as suitable.
#
if install != 'off':
    try:
        _cd0Name=instcdrom
    except:
        _cd0Name='MARKER_INSTSRCCDROM'

    if _cd0Name != '':
        if os.path.exists(_cd0Name):
            _cd0=_driver+':'+_cd0Name+','+_backend+'MARKER_HDD_c:cdrom,r'
        else:
            _cd0=''
            if mytrace == '1':
                print "mytrace:WARNING:Installmedia is currently not available"+_cd0Name+"'"
else:
    _cd0=''





#
#============================================================
#CD/DVD-1 - addcdrom
#============================================================
#
try:
    _cd1Name=addcdrom
except:
    _cd1Name=''

if _cd1Name != '':
    if os.path.exists(_cd1Name):
        _cd1=_driver+':'+_cd1Name+','+_backend+'MARKER_HDD_c:cdrom,r'
        if mytrace == '1':
            print 'mytrace: addcdrom=',_cd1Name
    else:
        _cd1=''
else:
    _cd1=''





#
#============================================================
#CONCAT MEDIA
#============================================================
#
disk=_diskList

if _cd0 != '':
     disk+=[_cd0,]

if _cd1 != '':
     disk+=[_cd1,]


if mytrace == '1':
    print 'mytrace: disk=',disk





#
#============================================================
#NET
#============================================================
#
dhcp='MARKER_DHCP'
try:
    _br0=bridge
except:
    _br0='MARKER_BRIDGE'

# xbridge=os.popen('ctys-getNetInfo.sh -X --bcx='+_br0).read()

# if xbridge == '':
#     print "WARNING:configured bridge("+_br0+") is no Xen bridge, scan for others."
#     xbridge=os.popen('ctys-getNetInfo.sh -X --blx').read()
#     if xbridge.__len__ > 1:
#         print "WARNING:Multiple Xen bridges("+xbridge+")"
#         _br0=(xbridge.split())[0]
#         print "WARNING:Take the first("+_br0+")"
#         _br0=_br0.strip()
#     elif xbridge.__len__ == 1:
#         _br0=xbridge.strip()
#     else :
#         raise "Missing bridge"

if dhcp != 'dhcp':
    if _builder == 'hvm' and _backendnet != '':
        vif = ['type='+_backendnet+', mac=MARKER_MAC, ip=MARKER_IP, bridge='+_br0]
    else:
        vif = ['mac=MARKER_MAC, ip=MARKER_IP, bridge='+_br0]
else:
    if _builder == 'hvm' and _backendnet != '':
        vif = ['type='+_backendnet+', mac=MARKER_MAC, bridge='+_br0]
    else:
        vif = ['mac=MARKER_MAC, bridge='+_br0]

if mytrace == '1':
    print 'mytrace: vif=',vif





#
#============================================================
#CONSOLE
#============================================================
#
serial='pty'

try:
    _s=con
except:
    con='sdl'

if mytrace == '1':
    print 'mytrace: con=',con

if con == 'nographic' con == 'textonly':
    sdl=0

    #
    #'want a vnc-attachment hook in any case!
    if con == 'textonly':
        vnc=0
    else:
        vnc = 1
        
        #do not want autostart, just the port presence for an eventual connect
        #vncconsole = 1
        
        #yes, fetching it dynamically call-by-call
        vncunused = 1
elif con == 'vnc':
    sdl = 0
    vnc = 1
    vncconsole = 1
    vncunused = 1

    if mytrace == '1':
        print 'mytrace: vncconsole =',vncconsole
        print 'mytrace: vncunused  =',vncunused

else:
    # elif con == 'sdl':
    sdl = 1
    #
    #'want a vnc-attachment hook in any case!
    #anyhow sdl-closes sessions so...
    vnc = 0
    vncconsole = 0
    vncunused = 0

    if mytrace == '1':
        print 'mytrace: vncconsole =',vncconsole
        print 'mytrace: vncunused  =',vncunused



if mytrace == '1':
    print 'mytrace: sdl        =',sdl
    print 'mytrace: vnc        =',vnc


# ffs.
# stdvga=0
# audio=1



#
#============================================================
#INSTALL-OR-OPERATIONS
#============================================================
#

#
# Use external kernel
_DomUkernel   = 'MARKER_DOMU_KERNEL'
if _builder == 'hvm':
    #
    # Expect now contained suitable kernel and probe for bootloader
    #
    # Pre-Configured user selection
    kernel  = "MARKER_BOOTLOADER"
    if not os.path.exists(kernel):
        #
        # Default-0:CentOS and any other upstream derivative, opensuse
        kernel = '/usr/lib/xen/boot/hvmloader'
        if not os.path.exists(kernel):
            #
            # debian and derivatives
            kernel = '/usr/' + arch_libdir + '/xen-default/boot/hvmloader'
            if not os.path.exists(kernel):
                #
                # for now unexpected!
                raise "Unsupported distribution, missing bootloader, no fallback:'MARKER_BOOTLOADER'!"

    if mytrace == '1':
        print 'mytrace: kernel=',kernel

elif install == 'off' and _DomUkernel == '':
    #
    # Expect now contained suitable kernel and probe for bootloader
    #
    # Pre-Configured user selection
    bootloader  = "MARKER_BOOTLOADER"
    if not os.path.exists(bootloader):
        #
        # Default-0:CentOS and any other upstream derivative, opensuse
        bootloader = '/usr/lib/xen/bin/pygrub'
        if not os.path.exists(bootloader):
            #
            # debian and derivatives
            bootloader = '/usr/' + arch_libdir + '/xen-default/bin/pygrub'
            if not os.path.exists(bootloader):
                #
                # for now unexpected!
                raise "Unsupported distribution, missing bootloader, no fallback:'MARKER_BOOTLOADER'!"

    if mytrace == '1':
        print 'mytrace: bootloader=',bootloader



if mytrace == '1':
    print 'mytrace: install=',install

if install != 'off':
   on_reboot = 'destroy'
   on_crash  = 'destroy'
   boot      = 'd'
   if _builder != 'hvm':
       kernel    = 'MARKER_INST_KERNEL'
       ramdisk   = 'MARKER_INST_INITRD'
       extra     = 'MARKER_INST_EXTRA'
       #
       # Attention: setting e.g. 'console=ttyS0' suppresses graphics output!
       root  = 'MARKER_INST_ROOTARGS'

       if mytrace == '1':
           print 'mytrace: kernel =',kernel
           print 'mytrace: ramdisk=',ramdisk
           print 'mytrace: extra  =',extra
else:
   on_reboot = 'restart'
   on_crash  = 'restart'
   boot      = 'c'

   if _builder != 'hvm' and _DomUkernel != '':
       kernel    = 'MARKER_DOMU_KERNEL'
       ramdisk   = 'MARKER_DOMU_INITRD'
       extra     = 'MARKER_DOMU_EXTRA'
       root      = 'MARKER_DOMU_ROOT'

       if kernelbase != '':
           kernel    = os.path.basename(kernel)
           kernel    = kernelbase+'/'+kernel
           ramdisk   = os.path.basename(ramdisk)
           ramdisk   = kernelbase+'/'+ramdisk


       if root == '':
           root='/dev/'+_hd_dev+' ro '

       if mytrace == '1':
           print 'mytrace: kernel =',kernel
           print 'mytrace: ramdisk=',ramdisk
           print 'mytrace: extra  =',extra
           print 'mytrace: root   =',root



if mytrace == '1':

    if _builder == 'hvm':
        print 'INFO:Use HVM-Virtualization '
    else:
        print 'INFO:Use PARA-Virtualization '

    if _DomUkernel != '':
        if _builder == 'hvm':
            print 'WARNING:Boot DomU by external kernel for hvm'
            print 'WARNING:Not sure whether this works.'
        else:
            print 'INFO:Boot DomU by external kernel'
    else:
        print 'INFO:Boot DomU by contained kernel'



